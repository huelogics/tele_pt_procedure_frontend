import {
  AppBar,
  Box,
  IconButton,
  Toolbar,
  Typography,
  Menu,
  MenuItem,
  Drawer,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import React, { useState, useRef, useEffect } from "react";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import NotificationsIcon from "@mui/icons-material/Notifications";
import EmailIcon from "@mui/icons-material/Email";
import Avatar from "@mui/material/Avatar";
import avatar from "../assets/avatar.png";
import companyLogo from "../assets/companyLogo.png";
import physicalTherapy from "../assets/physicalTherapy.png";
import ProcedureList from "./ProcedureList";
import AddProcedure from "./AddProcedure";
import ProcedureDetails from "./ProcedureDetails";
import axios from "axios";
import CircularProgress from "@mui/material/CircularProgress";
import AddExercise from "./AddExercise";
import ExerciseList from "./ExerciseList";
import Add from "./Add";
import ICFcaselist from "./ICFcaselist";
import AddIcfcase from "./AddIcfcase";
import AddIcfcase2 from "./AddIcfcase2";
import IcfDetailsAndCoreset from "./IcfDetailsAndCoreset";

const Dashboard = () => {
  const navigate = useNavigate();
  const [isAvatarMenuOpen, setIsAvatarMenuOpen] = useState(false);
  const [selectedTab, setSelectedTab] = useState("procedureList");
  const [isAddProcedureClicked, setIsAddProcedureClicked] = useState(false);
  const [selectedProcedure, setSelectedProcedure] = useState(null);
  const [isAddExerciseClicked, setIsAddExerciseClicked] = useState(false);
  const avatarRef = useRef(null);
  const [isExerciseListClicked, setIsExerciseListClicked] = useState(false);
  const [loading, setLoading] = useState(true);

  const [isICFCaseListClicked, setIsICFCaseListClicked] = useState(false);
  const [isAddICFCaseClicked, setAddIsICFCaseClicked] = useState(false);
  const [isAddICFCasepage2Clicked, setAddIsICFCasepage2Clicked] =
    useState(false);
  const [isICFcaseandCoresetclicked, setisICFcaseandCoresetclicked] =
    useState(false);
  const [issavebuttonofIcfandcoreetClicked, setIsICFcaseandCoresetclicked] =
    useState(false);

  // useEffect(() => {
  //   const token = localStorage.getItem("token");
  //   if (!token) {
  //     //   //  Redirect to login URL
  //     // navigate("/");
  //   } else {
  //     // Authentication check completed, stop loading
  //     setLoading(false);
  //   }
  // }, [navigate]);

  const handleAvatarClick = () => {
    setIsAvatarMenuOpen(!isAvatarMenuOpen); // Toggle menu open/close
  };
  const handleAddProcedureClick = () => {
    setIsAddProcedureClicked(true);
    setSelectedProcedure(null);
  };

  const handleProcedureClick = (procedure) => {
    setIsAddProcedureClicked(false); // Close add procedure form if open
    setSelectedProcedure(procedure); // Set selected procedure
  };

  const handleAddExerciseClick = () => {
    setIsAddExerciseClicked(true);
    setSelectedProcedure(null);
  };

  const handleIsExerciseListClicked = () => {
    setIsExerciseListClicked(true);
  };

  //to handle ICFcase list
  const handleICFcaseList = () => {
    setIsICFCaseListClicked(true);
    setIsExerciseListClicked(false);
    setIsAddProcedureClicked(false);
    setIsAddExerciseClicked(false);
    setAddIsICFCaseClicked(false);
    setSelectedProcedure(null);
  };

  //to handle AddIcfcaseclick
  const handleICFcaseclick = () => {
    setAddIsICFCaseClicked(true);
  };

  //to handle nextpage of Patientcase form
  const handlenextCasepage = () => {
    setAddIsICFCaseClicked(false);
    setAddIsICFCasepage2Clicked(true);
  };

  //to go back to patientcase form
  const handleBackbutton = () => {
    setAddIsICFCaseClicked(true);
    setAddIsICFCasepage2Clicked(false);
  };

  //to go to ICFandcoreset page
  const handleICFandCoresetbutton = () => {
    console.log("ICF and Coreset button clicked");
    setAddIsICFCaseClicked(false);
    setAddIsICFCasepage2Clicked(false);
    setisICFcaseandCoresetclicked(true);
  };

  //to go back on ICf list page
  const handleSavebuttonofIcfandcoreset = () => {
    setIsICFcaseandCoresetclicked(true);
    setAddIsICFCaseClicked(false);
    setAddIsICFCasepage2Clicked(false);
    setisICFcaseandCoresetclicked(false);
  };

  //logout
  const handlelogout = () => {
    setIsAvatarMenuOpen(false);

    const token = localStorage.getItem("token");
    // const refreshtoken = localStorage.getItem("refreshtoken");
    // console.log("TOKENNN", token);
    const config = {
      headers: {
        // token: token,
        Authorization: `Bearer ${token}`,
        // "X-CSRFToken": csrfToken
      },
    };

    const response = axios
      .post("https://procedures.huelogics.com/sign_out", {}, config)
      .then((response) => {
        localStorage.removeItem("token");
        localStorage.removeItem("refreshtoken");
        alert("Logout Successfylly !!!!");
        // console.log("LOGOUT SUCCESSFUL", response);
        navigate("/");
      })
      .catch((error) => {
        console.error("Logout failed:", error);
        // localStorage.removeItem("token");
        // localStorage.removeItem("refreshtoken");
        // navigate("/");
      });
  };

  const drawerWidth = "256px";

  const renderTabContent = () => {
    if (isAddICFCaseClicked) {
      return <AddIcfcase onNextCAsePage={handlenextCasepage} />;
    } else if (isAddICFCasepage2Clicked) {
      return (
        <AddIcfcase2
          onBack={handleBackbutton}
          onICFandCoreset={handleICFandCoresetbutton}
        />
      );
    } else if (isICFcaseandCoresetclicked) {
      return (
        <IcfDetailsAndCoreset Onsavebutton={handleSavebuttonofIcfandcoreset} />
      );
    } else if (isAddProcedureClicked) {
      return <AddProcedure />;
    } else if (selectedProcedure) {
      return <ProcedureDetails procedure={selectedProcedure} />;
    } else if (isAddExerciseClicked) {
      return <AddExercise />;
    } else if (isICFCaseListClicked || isICFcaseandCoresetclicked) {
      return <ICFcaselist onAddIcfcaseClick={handleICFcaseclick} />;
    } else if (isExerciseListClicked) {
      return (
        <ExerciseList
          onAddProcedureClick={handleAddProcedureClick}
          onAddExerciseClick={handleAddExerciseClick}
          onProcedureClick={handleProcedureClick}
        />
      );
    } else {
      return (
        <ProcedureList
          onAddProcedureClick={handleAddProcedureClick}
          onAddExerciseClick={handleAddExerciseClick}
          onProcedureClick={handleProcedureClick}
        />
      );
    }
  };

  // if (loading) {
  //   return (
  //     <div
  //       style={{
  //         display: "flex",
  //         justifyContent: "center",
  //         alignItems: "center",
  //         height: "100vh",
  //       }}
  //     >
  //       <CircularProgress />
  //     </div>
  //   );
  // }

  // const renderTabContent = () => {
  //   switch (selectedTab) {
  //     case "procedureList":
  //       return <ProcedureList />;
  //     //   case "agentTableDashboard":
  //     //     return <AgentTableDashboard />;
  //     //   case "emailDashboard":
  //     //     return <EmailDashboard />;

  //     default:
  //       return null;
  //   }
  // };
  return (
    <Box>
      {/*----------------- NAVBAR--------------------- */}
      <AppBar
        position="absolute"
        sx={{
          backgroundColor: "#FFFFFF",
          height: "55px",
          justifyContent: "center",
        }}
      >
        <Toolbar
          sx={{
            display: "flex",
            justifyContent: "flex-end",
            marginRight: "0.4rem",
            gap: "5px",
          }}
        >
          <div ref={avatarRef}>
            <IconButton onClick={handleAvatarClick}>
              <Avatar src={avatar} sx={{ width: "35px", height: "33px" }} />
            </IconButton>
            <Menu
              anchorEl={avatarRef.current}
              open={isAvatarMenuOpen}
              onClose={() => setIsAvatarMenuOpen(false)}
              disableScrollLock={true}
            >
              <MenuItem onClick={handlelogout}>Logout</MenuItem>
            </Menu>
          </div>
          <Box
            sx={{
              color: "#000000",
              width: "42px",
              height: "23px",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Typography
              sx={{
                fontFamily: "Poppins",
                fontWeight: "700",
                fontSize: "10px",
              }}
            >
              Dr. Test
            </Typography>
            <Typography
              sx={{ fontFamily: "Poppins", fontWeight: "400", fontSize: "7px" }}
            >
              Therapist
            </Typography>
          </Box>
          <IconButton>
            <NotificationsIcon sx={{ color: "#000000", fontSize: "20px" }} />
          </IconButton>
          <IconButton>
            <EmailIcon sx={{ color: "#000000", fontSize: "20px" }} />
          </IconButton>
        </Toolbar>
      </AppBar>

      {/*-------------- Sidebar-------------- */}
      <Drawer
        variant="permanent"
        anchor="left"
        PaperProps={{
          sx: {
            width: drawerWidth,
            color: "#FFF",
            flexShrink: 0,
            // backgroundImage:
            //   "linear-gradient(to right, #48C6EF 0%, #6F86D6 100%)",
            background: "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
            zIndex: 9999,
          },
        }}
      >
        <div
          style={{
            textAlign: "center",
            marginTop: "10px",
          }}
        >
          <img
            src={companyLogo}
            alt="Company Logo"
            style={{
              width: "77px",
              height: "61px",
              //   marginBottom: "-9px",
            }}
          />
          <div
            style={{
              fontSize: "20px",
              fontWeight: "500",
              color: "#FFFFFF",
              fontFamily: "Poppins",
            }}
          >
            HUE LOGICS
          </div>
        </div>

        <List
          sx={{
            color: "white",
            marginTop: "107px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
            gap: "0.8rem",
          }}
        >
          <ListItemButton
            sx={{
              display: "flex",
              alignItems: "center",
              //   padding: "0px",
              justifyContent: "center",
              borderRadius: "10px",
              width: "211px",
              height: "42px",
              border: "1px solid #FFFFFF",
              //   border:
              // selectedTab === "countDashboard" && openAccountDrawer === false
              //   ? "4px solid rgba(255, 255, 255, 0.61)"
              //   : "none",
            }}
            onClick={() => {
              setIsExerciseListClicked(false);
              setIsAddProcedureClicked(false);
              setIsAddExerciseClicked(false);
              setIsICFCaseListClicked(false);
              setAddIsICFCaseClicked(false);
              setSelectedProcedure(null);
            }}
            // onClick={() => handleTabChange("countDashboard")}
          >
            <ListItemIcon sx={{ minWidth: "auto", marginRight: "8px" }}>
              <img src={physicalTherapy} />
            </ListItemIcon>
            {/* <ListItemText
              sx={{
                // width: "112px",
                fontFamily: "Poppins",
                fontSize: "20px",
                fontWeight: "500",
              }}
            > */}
            <div
              style={{
                fontFamily: "Poppins",
                fontSize: "17px",
                fontWeight: "400",
              }}
            >
              Procedure
            </div>

            {/* </ListItemText> */}
          </ListItemButton>

          <ListItemButton
            sx={{
              display: "flex",
              alignItems: "center",
              //   padding: "0px",
              justifyContent: "center",
              borderRadius: "10px",
              width: "211px",
              height: "42px",
              border: "1px solid #FFFFFF",
              //   border:
              // selectedTab === "countDashboard" && openAccountDrawer === false
              //   ? "4px solid rgba(255, 255, 255, 0.61)"
              //   : "none",
            }}
            onClick={() => {
              handleIsExerciseListClicked();
              setIsAddProcedureClicked(false);
              setIsAddExerciseClicked(false);
              setIsICFCaseListClicked(false);
              setAddIsICFCaseClicked(false);
              setSelectedProcedure(null);
            }}
            // onClick={() => handleTabChange("countDashboard")}
          >
            <ListItemIcon sx={{ minWidth: "auto", marginRight: "8px" }}>
              <img src={physicalTherapy} />
            </ListItemIcon>
            <div
              style={{
                fontFamily: "Poppins",
                fontSize: "17px",
                fontWeight: "400",
              }}
            >
              Exercise
            </div>
          </ListItemButton>

          <ListItemButton
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              borderRadius: "10px",
              width: "211px",
              height: "42px",
              border: "1px solid #FFFFFF",
            }}
            onClick={handleICFcaseList}
          >
            <ListItemIcon sx={{ minWidth: "auto", marginRight: "8px" }}>
              <img src={physicalTherapy} />
            </ListItemIcon>
            <div
              style={{
                fontFamily: "Poppins",
                fontSize: "17px",
                fontWeight: "400",
              }}
            >
              ICF case
            </div>
          </ListItemButton>
        </List>
        {/* <Divider /> */}

        {/* <Divider /> */}
      </Drawer>

      {/*-------------------- MAIN BODY------------------- */}
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          bgcolor: "background.default",
          // p: 3,
          marginLeft: drawerWidth,
          // backgroundColor: "#",
        }}
      >
        <Toolbar />
        {renderTabContent()}
      </Box>
    </Box>
  );
};

export default Dashboard;
