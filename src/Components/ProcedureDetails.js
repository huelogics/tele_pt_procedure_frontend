import React, { useEffect, useState } from "react";
import {
  Grid,
  Box,
  Typography,
  TextField,
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  IconButton,
  Button,
  Menu,
  MenuItem,
} from "@mui/material";
import procedureDiagram from "../../src/assets/procedureDiagram.png";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import addIcon from "../assets/addIcon.png";
import axios from "axios";
import { Link as RouterLink, useNavigate } from "react-router-dom";

const ProcedureDetails = ({ procedure }) => {
  const data = procedure;
  console.log("PROCEDURE DETAIL", data);
  const procedureID = data.procedure_id;
  const procedureName = data?.name || "Unknown Procedure";
  // console.log(procedureName);
  const procedureDetails = data?.details || "No details available";
  console.log(procedureDetails);
  const joints = data?.joints || [];
  const landmarks = data?.landmarks || [];
  const pivots = data?.pivot_landmark || [];
  const options = data?.options || [];
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedOption, setSelectedOption] = useState(null);

  //for drop down list open
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  //for drop down list close
  const handleClose = () => {
    setAnchorEl(null);
  };

  //dropdown for option button
  const handleOptionSelect = (option) => {
    setSelectedOption(option);
    handleClose();
  };

  return (
    <Box sx={{ padding: "15px 29px" }}>
      <Grid
        container
        // spacing={4}
        sx={{
          display: "flex",
          flexDirection: "row",
          gap: "30px",
        }}
      >
        <Grid item xs={3}>
          <Typography
            sx={{
              fontFamily: "Inter",
              fontWeight: "700",
              fontSize: "32px",
              color: "#000000",
              mt: "-3px",
            }}
          >
            {procedureName}
          </Typography>

          <Typography
            sx={{
              fontFamily: "Montserrat",
              fontSize: "13px",
              fontWeight: "500",
              color: "#000000",
              mt: "17px",
              mb: "8px",
            }}
          >
            Procedure Details
          </Typography>
          <TextField
            type="text"
            multiline
            fullWidth
            value={procedureDetails}
            sx={{
              // width: "310px",
              width: "100%",
              "& .MuiOutlinedInput-root": {
                "& fieldset": {
                  border: "none",
                },
              },
              border: "1px solid #9E9E9E",
              height: `calc(100vh - 187px)`,
            }}
            inputProps={{
              style: {
                fontFamily: "Inter",
                fontWeight: 400,
                fontSize: "13px",
                color: "#000000",
              },
            }}
          ></TextField>
        </Grid>
        <Grid
          item
          xs={8.5}
          sx={{ display: "flex", flexDirection: "column", gap: "10px" }}
        >
          <Grid>
            <Button
              endIcon={<KeyboardArrowDownIcon />}
              onClick={handleClick}
              sx={{
                backgroundImage:
                  "linear-gradient(to right, #667EEA 0%, #764BA2 100%)",
                color: "#FFFFFF",
                borderRadius: "50px",
                fontFamily: "Inter",
                fontSize: "14px",
                fontWeight: "500",
                textTransform: "none",
                width: "148px",
                height: "40px",
                mt: "3px",
              }}
            >
              Option/Type
            </Button>
            <Menu
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              {options.length === 0 ? (
                <MenuItem
                  sx={{
                    width: "148px",
                    fontFamily: "Inter",
                    fontSize: "14px",
                  }}
                >
                  No options
                </MenuItem>
              ) : (
                options.map((option, index) => (
                  <MenuItem
                    sx={{
                      width: "148px",
                      fontFamily: "Inter",
                      fontSize: "14px",
                    }}
                    key={index}
                    onClick={() => handleOptionSelect(option)}
                  >
                    {option}
                  </MenuItem>
                ))
              )}
            </Menu>
          </Grid>
          <Typography
            sx={{
              fontFamily: "Montserrat",
              fontSize: "13px",
              fontWeight: "500",
              color: "#000000",
              mt: "8px",
              // mb: "8px",
            }}
          >
            Reference Diagram
          </Typography>
          <Box
            sx={
              {
                // mt: "10px",
                // border: "1px solid #9E9E9E",
                // height: `calc(100vh - 400px)`,
              }
            }
          >
            <img
              src={procedureDiagram}
              style={{
                height: `calc(100vh - 370px)`,
                width: "850px",
              }}
              // style={{ height: "350px", width: "850px" }}
            ></img>
          </Box>
          <Grid
            // item
            xs={12}
            sx={{
              display: " flex",
              gap: "40px",
            }}
          >
            <Grid
              xs={4}
              sx={{
                display: "flex",
                alignItems: "flex-start",
              }}
            >
              <TableContainer>
                <div style={{ marginBottom: "2px" }}>
                  <Table padding={"none"}>
                    <TableHead sx={{ height: "35px", marginBottom: "30px" }}>
                      <TableRow>
                        <TableCell
                          sx={{
                            background:
                              "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",

                            color: "#FFFFFF",
                            borderRadius: "10px",
                          }}
                        >
                          <Box
                            sx={{
                              display: "flex",
                              flexDirection: "row",
                              justifyContent: "center",
                            }}
                          >
                            <Typography
                              sx={{
                                fontFamily: "Inter",
                                fontWeight: "Bold",
                                fontSize: "14px",
                              }}
                            >
                              Landmarks
                            </Typography>
                          </Box>
                        </TableCell>
                      </TableRow>
                    </TableHead>
                  </Table>
                </div>

                <div style={{ marginTop: "2px" }}>
                  <Table>
                    <TableBody sx={{ marginTop: "30px" }}>
                      {/* {[...Array(3)].map((_, index) => ( */}
                      {landmarks.length === 0 ? (
                        <TableRow
                          sx={{
                            height: "44px",
                            color: "#101828",
                            fontFamily: "Inter",
                          }}
                        >
                          <TableCell
                            align="center"
                            sx={{
                              padding: "12px 24px",
                              fontSize: "14px",
                              fontFamily: "Inter",
                              border: "1px solid rgba(51, 51, 51, 0.17)",
                            }}
                          >
                            {/* Empty cell */}
                          </TableCell>
                        </TableRow>
                      ) : (
                        landmarks.map((landmark, index) => (
                          <TableRow
                            key={index}
                            sx={{
                              height: "44px",
                              color: "#101828",
                              fontFamily: "Inter",
                            }}
                          >
                            <TableCell
                              align="center"
                              sx={{
                                padding: "12px 24px",
                                // fontWeight: "500",
                                fontSize: "14px",
                                fontFamily: "Inter",
                                border: "1px solid rgba(51, 51, 51, 0.17)",
                              }}
                            >
                              {landmark}
                            </TableCell>
                          </TableRow>
                        ))
                      )}
                    </TableBody>
                  </Table>
                </div>
              </TableContainer>
              <IconButton>
                <img
                  style={{ width: "18px", height: "18px" }}
                  src={addIcon}
                ></img>
              </IconButton>
            </Grid>
            <Grid
              xs={4}
              sx={{
                display: "flex",
                alignItems: "flex-start",
              }}
            >
              <TableContainer>
                <div style={{ marginBottom: "2px" }}>
                  <Table padding={"none"}>
                    <TableHead sx={{ height: "35px", marginBottom: "30px" }}>
                      <TableRow>
                        <TableCell
                          sx={{
                            background:
                              "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",

                            color: "#FFFFFF",
                            borderRadius: "10px",
                          }}
                        >
                          <Box
                            sx={{
                              display: "flex",
                              flexDirection: "row",
                              justifyContent: "center",
                            }}
                          >
                            <Typography
                              sx={{
                                fontFamily: "Inter",
                                fontWeight: "Bold",
                                fontSize: "14px",
                              }}
                            >
                              Joints
                            </Typography>
                          </Box>
                        </TableCell>
                      </TableRow>
                    </TableHead>
                  </Table>
                </div>

                <div style={{ marginTop: "2px" }}>
                  <Table>
                    <TableBody sx={{ marginTop: "30px" }}>
                      {joints.length === 0 ? (
                        <TableRow
                          sx={{
                            height: "44px",
                            color: "#101828",
                            fontFamily: "Inter",
                          }}
                        >
                          <TableCell
                            align="center"
                            sx={{
                              padding: "12px 24px",
                              fontSize: "14px",
                              fontFamily: "Inter",
                              border: "1px solid rgba(51, 51, 51, 0.17)",
                            }}
                          >
                            {/* Empty cell */}
                          </TableCell>
                        </TableRow>
                      ) : (
                        joints.map((joint, index) => (
                          <TableRow
                            key={index}
                            sx={{
                              height: "44px",
                              color: "#101828",
                              fontFamily: "Inter",
                            }}
                          >
                            <TableCell
                              align="center"
                              sx={{
                                padding: "12px 24px",
                                // fontWeight: "500",
                                fontSize: "14px",
                                fontFamily: "Inter",
                                border: "1px solid rgba(51, 51, 51, 0.17)",
                              }}
                            >
                              {joint}
                            </TableCell>
                          </TableRow>
                        ))
                      )}
                    </TableBody>
                  </Table>
                </div>
              </TableContainer>
              <IconButton>
                <img
                  style={{ width: "18px", height: "18px" }}
                  src={addIcon}
                ></img>
              </IconButton>
            </Grid>
            <Grid
              xs={4}
              sx={{
                display: "flex",
                alignItems: "flex-start",
              }}
            >
              <TableContainer>
                <div style={{ marginBottom: "2px" }}>
                  <Table padding={"none"}>
                    <TableHead sx={{ height: "35px", marginBottom: "30px" }}>
                      <TableRow>
                        <TableCell
                          sx={{
                            background:
                              "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",

                            color: "#FFFFFF",
                            borderRadius: "10px",
                          }}
                        >
                          <Box
                            sx={{
                              display: "flex",
                              flexDirection: "row",
                              justifyContent: "center",
                            }}
                          >
                            <Typography
                              sx={{
                                fontFamily: "Inter",
                                fontWeight: "Bold",
                                fontSize: "14px",
                              }}
                            >
                              Pivot Joints
                            </Typography>
                          </Box>
                        </TableCell>
                      </TableRow>
                    </TableHead>
                  </Table>
                </div>

                <div style={{ marginTop: "2px" }}>
                  <Table>
                    <TableBody sx={{ marginTop: "30px" }}>
                      {/* {pivots.map((pivot, index) => ( */}
                      <TableRow
                        // key={index}
                        sx={{
                          height: "44px",
                          color: "#101828",
                          fontFamily: "Inter",
                        }}
                      >
                        <TableCell
                          align="center"
                          sx={{
                            padding: "12px 24px",
                            // fontWeight: "500",
                            fontSize: "14px",
                            fontFamily: "Inter",
                            border: "1px solid rgba(51, 51, 51, 0.17)",
                          }}
                        >
                          {pivots}
                        </TableCell>
                      </TableRow>
                      {/* ))} */}
                    </TableBody>
                  </Table>
                </div>
              </TableContainer>

              <IconButton>
                <img
                  style={{ width: "18px", height: "18px" }}
                  src={addIcon}
                ></img>
              </IconButton>
            </Grid>
          </Grid>
        </Grid>
        {/* <Grid item xs={12}>
      <Typography
        sx={{
          fontFamily: "Montserrat",
          fontWeight: "500",
          fontSize: "10px",
          color: "#333333",
        }}
      >
        Procedure Name
      </Typography>
      <Paper sx={{ width: "310px", mt: "10px" }}>
        <TextField
          sx={{
            width: "300px",
            "& .MuiOutlinedInput-root": {
              "& fieldset": {
                border: "none",
              },
            },
          }}
          placeholder="Type here"
          inputProps={{
            style: {
              fontFamily: "Montserrat",
              fontWeight: 400,
              fontSize: "13px",
              color: "#000000",
              padding: "15px 19px",
            },
          }}
        ></TextField>
      </Paper>
    </Grid>
    <Typography
      sx={{
        fontFamily: "Montserrat",
        fontSize: "10px",
        fontWeight: "500",
        color: "#000000",
        mt: "10px",
        mb: "8px",
      }}
    >
      Procedure Details
    </Typography>
    <Grid
      sx={{ display: "flex", flexDirection: "row", gap: "29px" }}
      container
    >
      <Grid item xs={3.05}>
        <TextField
          type="text"
          multiline
          sx={{
            width: "310px",
            "& .MuiOutlinedInput-root": {
              "& fieldset": {
                border: "none",
              },
            },
            border: "1px solid #9E9E9E",
            height: `calc(100vh - 220px)`,
          }}
          inputProps={{
            style: {
              fontFamily: "Montserrat",
              fontWeight: 400,
              fontSize: "13px",
              color: "#000000",
            },
          }}
        ></TextField>
      </Grid>
      <Grid
        item
        xs={8.6}
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: "10px",
        }}
      >
        <Grid>
          <Box
            sx={
              {
                // border: "1px solid #9E9E9E",
                // height: `calc(100vh - 400px)`,
              }
            }
          >
            <img
              src={procedureDiagram}
              style={{ height: "350px", width: "850px" }}
            ></img>
          </Box>
        </Grid>
        <Grid>
          <Box></Box>
        </Grid>
      </Grid>
    </Grid> */}
      </Grid>
    </Box>
  );
};

export default ProcedureDetails;
