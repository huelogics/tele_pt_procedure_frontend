import {
  Box,
  Button,
  Grid,
  InputBase,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Pagination,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import SearchIcon from "@mui/icons-material/Search";
import PaginationItem from "@mui/material/PaginationItem";
import AddBoxIcon from "@mui/icons-material/AddBox";
import { styled } from "@mui/material/styles";
import DownloadIcon from "../assets/downloadIcon.png";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import axios from "axios";

const Search = styled("div")(({ theme }) => ({
  borderRadius: "20px",
  border: "1px solid #9E9E9E",
  width: { md: "180px", lg: "290px" },
  height: "32px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  marginLeft: 0,
}));

const ICFcaselist = ({ onAddIcfcaseClick }) => {
  const navigate = useNavigate();
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage] = useState(10);
  const [icfcaseresponse, setIcfcaseresponse] = useState([]);
  const [filterIcfcaseresponse, setFilterIcfcaseresponse] = useState([]);

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (!token) {
      //   //  Redirect to login URL
      navigate("/");
      fetchExerciseList();
    } else {
      fetchExerciseList();
    }
  }, []);

  const fetchExerciseList = async () => {
    const token = localStorage.getItem("token");
    // console.log("Token", token);
    try {
      const IcfandcoresetListResponse = await axios.get(
        "https://procedures.huelogics.com/get_records_added_by_therapist",
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      console.log("ICF and coreset List Response", IcfandcoresetListResponse);
      setIcfcaseresponse(IcfandcoresetListResponse);
      // setFilterIcfcaseresponse(IcfandcoresetListResponse.data);

      // setEmailResponse(allEmailsResponse.data);
      // setFilteredEmailResponse(allEmailsResponse.data);

      // setDepartmentResponse(allEmailsResponse.data.department);
    } catch (err) {
      console.log("Error in fetching procedures list", err);
    }
  };

  console.log(icfcaseresponse);

  // Pagination logic
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = filterIcfcaseresponse.slice(
    indexOfFirstItem,
    indexOfLastItem
  );
  // const paginate = (pageNumber) => setCurrentPage(pageNumber);
  const handleChangePage = (event, newPage) => {
    setCurrentPage(newPage);
    event.preventDefault();
  };

  return (
    <Box
      sx={{
        height: `calc(100vh - 64px)`,
        display: "flex", //for sticky pagination
        flexDirection: "column", //fsp
      }}
    >
      <Grid container sx={{ padding: { xs: "20px 0px", sm: "20px 25px" } }}>
        <Grid
          item
          xs={2}
          lg={2.2}
          sx={{
            display: "flex",
            justifyContent: "flex-start",
            alignItems: "flex-start",
            // backgroundColor: {
            //   xl: "pink",
            //   lg: "violet",
            //   md: "yellow",
            //   sm: "red",
            // },
          }}
        >
          <Typography
            sx={{
              // fontSize: "32px",
              fontSize: { md: "20px", lg: "32px" },
              fontWeight: 700,
              fontFamily: "Poppins",
            }}
          >
            ICF Case
          </Typography>
        </Grid>
        <Grid
          item
          md={5}
          lg={4.8}
          // justifyContent="center"
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            // backgroundColor: "yellowgreen",
          }}
        >
          <Search sx={{ gap: "8px" }}>
            <SearchIcon
              sx={{
                color: "#9E9E9E",
                width: "16px",
                height: "16px",
                padding: "10px",
              }}
            />
            <InputBase
              placeholder="Search.."
              inputProps={{ "aria-label": "search" }}
              sx={{
                color: "#9E9E9E",
                fontSize: "12px",
                fontFamily: "Montserrat",
                // width: "248px",
                width: { xs: "100px", md: "150px", lg: "300px" },
                height: "15px",
              }}
              //   value={searchQuery}
              // //   onChange={(e) => handleSearch(e.target.value)}
            />
          </Search>
        </Grid>

        <Grid
          item
          md={5}
          lg={5}
          container
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "right",
            // paddingRight: "4rem",
            alignItems: "center",
            gap: { sm: "10px", lg: "30px" },
            // backgroundColor: "violet",
          }}
        >
          <Button
            variant="outlined"
            startIcon={<img src={DownloadIcon} alt="Download Icon" />}
            sx={{
              color: "#FFFFFF",
              border: "none",
              borderRadius: "50px",
              //   width: "102px",
              height: "40px",
              padding: { md: "10px 15px", lg: "10px 26px" },
              //   gap: "0.5px",
              backgroundImage:
                "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
              "&:hover": {
                // backgroundColor: "#FCFCFD",
                border: "none",
              },
            }}
            // onClick={handleClickFilter}
          >
            <Typography
              sx={{
                color: "#FFFFFF",
                fontSize: "13px",
                fontFamily: "Poppins",
                fontWeight: 500,
                textTransform: "none",
              }}
            >
              Download
            </Typography>
          </Button>

          <Button
            variant="outlined"
            startIcon={<AddBoxIcon />}
            sx={{
              color: "#FFFFFF",
              border: "none",
              borderRadius: "50px",
              //   width: "102px",
              height: "40px",
              padding: { md: "10px 15px", lg: "10px 26px" },
              gap: "0.5px",
              backgroundImage:
                "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
              "&:hover": {
                border: "none",
              },
            }}
            onClick={onAddIcfcaseClick}
          >
            <Typography
              sx={{
                color: "#FFFFFF",
                fontSize: "13px",
                fontFamily: "Poppins",
                fontWeight: 500,
                textTransform: "none",
              }}
            >
              ICF case
            </Typography>
          </Button>
        </Grid>
      </Grid>

      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          padding: "0px 33px",
          flexGrow: 1, //fsp
        }}
      >
        <TableContainer
          sx={{
            borderRadius: "0px",
            flexGrow: 1, //fsp
          }}
        >
          <Table padding={"none"}>
            <TableHead sx={{ height: "44px" }}>
              <TableRow>
                <TableCell
                  sx={{
                    width: "20%",
                    background: "#6F86D6",
                    color: "#FFFFFF",
                    border: "1px solid #323C47",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <Typography
                      sx={{
                        fontFamily: "Poppins",
                        fontWeight: "500",
                        fontSize: "14px",
                      }}
                    >
                      Case Name
                    </Typography>
                  </Box>
                </TableCell>
                <TableCell
                  align="center"
                  sx={{
                    width: "20%",
                    background: "#6F86D6",
                    color: "#FFFFFF",
                    border: "1px solid #323C47",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <Typography
                      sx={{
                        fontFamily: "Poppins",
                        fontWeight: "500",
                        fontSize: "14px",
                      }}
                    >
                      Therapist Id
                    </Typography>
                  </Box>
                </TableCell>
                <TableCell
                  align="center"
                  sx={{
                    width: "20%",
                    background: "#6F86D6",
                    color: "#FFFFFF",
                    border: "1px solid #323C47",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <Typography
                      sx={{
                        fontFamily: "Poppins",

                        fontWeight: "500",
                        fontSize: "14px",
                      }}
                    >
                      Therapist
                    </Typography>
                  </Box>
                </TableCell>
                <TableCell
                  align="center"
                  sx={{
                    width: "20%",
                    border: "1px solid #323C47",
                    background: "#6F86D6",
                    color: "#FFFFFF",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                  >
                    <Typography
                      sx={{
                        fontFamily: "Poppins",
                        fontWeight: "500",
                        fontSize: "14px",
                      }}
                    >
                      Date
                    </Typography>
                  </Box>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {/* {filterIcfcaseresponse &&
                filterIcfcaseresponse.map((val, ind) => {
                  console.log("FILTERES", filterIcfcaseresponse); */}

              {currentItems.map((val, ind) => {
                return (
                  <TableRow
                    key={ind}
                    sx={{
                      height: "44px",
                      color: "#101828",
                      fontFamily: "Poppins",
                      cursor: "pointer",
                    }}
                  >
                    <TableCell
                      align="center"
                      sx={{
                        // padding: "12px 24px",
                        // fontWeight: "500",
                        fontFamily: "Poppins",
                        fontSize: "14px",
                        border: "1px solid #9E9E9E",
                      }}
                    >
                      {val.name}
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        padding: "12px 24px",
                        fontFamily: "Poppins",
                        fontSize: "14px",
                        border: "1px solid #9E9E9E",
                      }}
                    >
                      {val.therapist_id}
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        padding: "12px 24px",
                        // fontWeight: "500",
                        fontSize: "14px",
                        border: "1px solid #9E9E9E",
                        fontFamily: "Poppins",
                      }}
                    >
                      {val.therapist}
                    </TableCell>
                    <TableCell
                      align="center"
                      sx={{
                        padding: "12px 24px",
                        // fontWeight: "500",
                        fontSize: "14px",
                        border: "1px solid #9E9E9E",
                        fontFamily: "Poppins",
                      }}
                    >
                      {val.date}
                    </TableCell>
                  </TableRow>
                );
              })}
              {/* })} */}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>

      {/*------------------------ Pagination------------------ */}
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          marginTop: "21px",
          // marginBottom: 0,
          marginBottom: "21px", //fsp
        }}
      >
        <Pagination
          count={Math.ceil(filterIcfcaseresponse.length / itemsPerPage)}
          page={currentPage}
          onChange={handleChangePage}
          renderItem={(item) => {
            const isActivePage =
              item.type === "page" && item.page === currentPage;
            if (item.type === "page") {
              return (
                <PaginationItem
                  {...item}
                  component={IconButton}
                  sx={{
                    borderRadius: "8px",
                    backgroundImage: isActivePage
                      ? "linear-gradient(to right,#667EEA 0%, #764BA2 100%)"
                      : "linear-gradient(to right, #E0E0E0, #E0E0E0)",
                    color: isActivePage ? "#FFFFFF" : "#000000",
                    fontSize: "12px",
                    fontFamily: "Montserrat",
                    fontWeight: "500",
                    width: "31px",
                    height: "31px",
                  }}
                />
              );
            }
            if (item.type === "previous") {
              return (
                <Typography
                  {...item}
                  component={IconButton}
                  disabled={currentPage === 1}
                  onClick={() => setCurrentPage(currentPage - 1)}
                  sx={{
                    borderRadius: "8px",
                    fontSize: "12px",
                    fontWeight: "500",
                    fontFamily: "Montserrat",
                  }}
                >
                  Previous
                </Typography>
              );
            }
            if (item.type === "next") {
              return (
                <Typography
                  {...item}
                  component={IconButton}
                  disabled={
                    currentPage ===
                    Math.ceil(filterIcfcaseresponse.length / itemsPerPage)
                  }
                  onClick={() => setCurrentPage(currentPage + 1)}
                  sx={{
                    borderRadius: "8px",
                    fontSize: "12px",
                    fontWeight: "500",
                    fontFamily: "Montserrat",
                  }}
                >
                  Next
                </Typography>
              );
            }
          }}
        />
      </Box>
    </Box>
  );
};

export default ICFcaselist;
