import {
  Grid,
  Box,
  Typography,
  TextField,
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  IconButton,
  Button,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import procedureDiagram from "../../src/assets/procedureDiagram.png";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import addIcon from "../assets/addIcon.png";
import axios from "axios";

const AddProcedure = () => {
  const [procedureName, setProcedureName] = useState("");
  const [procedureDetails, setProcedureDetails] = useState("");
  const [landmarks, setLandmarks] = useState(Array(3).fill(""));
  // const [joints, setJoints] = useState(Array(3).fill([""]));
  const [joints, setJoints] = useState(Array(3).fill(Array(2).fill("")));
  const [pivotJoints, setPivotJoints] = useState(Array(3).fill(""));
  const [options, setOptions] = useState(Array(3).fill(""));

  //for adding new row after clicking on add icon
  const handleAddRow = (section) => {
    if (section.label === "Joints") {
      section.setData((prevState) => [
        ...prevState,
        ["", ""], // Add a new row with two empty strings for Joints
      ]);
    } else {
      // Add logic for adding rows to other sections here
      section.setData((prevState) => [...prevState, ""]);
    }
    // setState((prevState) => [...prevState, ""]);
  };

  //handler  for submitting form
  const handleSave = () => {
    // Convert joints data into the desired array format
    // const formattedJoints = joints.map((joint) => joint.split("_"));
    // const formattedJoints = joints.map((joint) =>
    //   joint.split(",").map((item) => item.trim())
    // );
    // joints: formattedJoints,

    // Convert pivotJoints array to string
    const pivotJointsString = pivotJoints.join(", ");
    const data = {
      name: procedureName,
      details: procedureDetails,
      landmarks,
      joints,
      pivot_landmark: pivotJointsString,
      options,
    };

    console.log("Request Body", data);
    const token = localStorage.getItem("token");

    axios
      .post("https://procedures.huelogics.com/add_procedure", data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((response) => {
        console.log("Procedure added successfully:", response.data);
        alert("Data has been saved!!");
      })
      .catch((error) => {
        console.error("Error adding procedure:", error.response.data);
      });
  };

  // const [rows, setRows] = useState([...Array(3)].map(() => ""));
  // const [jointsRows, setJointsRows] = useState([...Array(3)].map(() => ""));
  // const [pivotsRows, setPivotsRows] = useState([...Array(3)].map(() => ""));
  // const [optionsRows, setOptionsRows] = useState([...Array(3)].map(() => ""));

  // const handleAddRow = () => {
  //   setRows([...rows, ""]);
  // };

  // const handleJointRow = () => {
  //   setJointsRows([...jointsRows, ""]);
  // };

  // const handlePivotRow = () => {
  //   setPivotsRows([...pivotsRows, ""]);
  // };

  // const handleOptionRow = () => {
  //   setOptionsRows([...optionsRows, ""]);
  // };

  return (
    <Box sx={{ padding: "15px 29px" }}>
      <Grid
        container
        // spacing={4}
        sx={{
          display: "flex",
          flexDirection: "row",
          gap: "30px",
        }}
      >
        <Grid item xs={3}>
          <Typography
            sx={{
              fontFamily: "Montserrat",
              fontWeight: "500",
              fontSize: "13px",
              color: "#000000 ",
            }}
          >
            Procedure Name
          </Typography>
          <Paper
            sx={{
              // width: "310px",
              mt: "10px",
            }}
          >
            <TextField
              fullWidth
              value={procedureName}
              onChange={(e) => setProcedureName(e.target.value)}
              sx={{
                // width: "310px",
                // width: "100%",
                "& .MuiOutlinedInput-root": {
                  "& fieldset": {
                    border: "none",
                  },
                },
              }}
              placeholder="Type here"
              inputProps={{
                style: {
                  fontFamily: "Montserrat",
                  fontWeight: 400,
                  fontSize: "13px",
                  color: "#000000",
                  padding: "15px 19px",
                },
              }}
            ></TextField>
          </Paper>
          <Typography
            sx={{
              fontFamily: "Montserrat",
              fontSize: "13px",
              fontWeight: "500",
              color: "#000000",
              mt: "10px",
              mb: "8px",
            }}
          >
            Procedure Details
          </Typography>
          <TextField
            type="text"
            multiline
            fullWidth
            value={procedureDetails}
            onChange={(e) => setProcedureDetails(e.target.value)}
            sx={{
              // width: "310px",
              width: "100%",
              "& .MuiOutlinedInput-root": {
                "& fieldset": {
                  border: "none",
                },
              },
              border: "1px solid #9E9E9E",
              height: `calc(100vh - 220px)`,
            }}
            inputProps={{
              style: {
                fontFamily: "Montserrat",
                fontWeight: 400,
                fontSize: "13px",
                color: "#000000",
              },
            }}
          ></TextField>
        </Grid>
        <Grid
          item
          xs={8.5}
          sx={{ display: "flex", flexDirection: "column", gap: "10px" }}
        >
          <Box
            sx={{
              mt: "10px",
              // border: "1px solid #9E9E9E",
              // height: `calc(100vh - 400px)`,
            }}
          >
            <img
              src={procedureDiagram}
              style={{
                height: `calc(100vh - 370px)`,
                width: "850px",
                // maxWidth: `calc(100%-450px)`,
              }}
              // style={{ height: "350px", width: "850px" }}
            ></img>
          </Box>
          <Grid
            item
            xs={12}
            sx={{
              display: " flex",
              gap: "15px",
            }}
          >
            {[
              { data: landmarks, setData: setLandmarks, label: "Landmarks" },
              { data: joints, setData: setJoints, label: "Joints" },
              {
                data: pivotJoints,
                setData: setPivotJoints,
                label: "Pivot Joints",
              },
              { data: options, setData: setOptions, label: "Options/Type" },
            ].map((section, index) => (
              <Grid
                xs={3}
                key={index}
                sx={{
                  display: "flex",
                  alignItems: "flex-start",
                }}
              >
                <TableContainer>
                  <div style={{ marginBottom: "2px" }}>
                    <Table padding={"none"}>
                      <TableHead sx={{ height: "35px", marginBottom: "30px" }}>
                        <TableRow>
                          <TableCell
                            sx={{
                              background:
                                "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
                              color: "#FFFFFF",
                              borderRadius: "10px",
                            }}
                          >
                            <Box
                              sx={{
                                display: "flex",
                                flexDirection: "row",
                                justifyContent: "center",
                              }}
                            >
                              <Typography
                                sx={{
                                  fontFamily: "Poppins",
                                  fontWeight: "Bold",
                                  fontSize: "14px",
                                }}
                              >
                                {section.label}
                              </Typography>
                            </Box>
                          </TableCell>
                        </TableRow>
                      </TableHead>
                    </Table>
                  </div>

                  <div style={{ marginTop: "2px" }}>
                    <Table>
                      {section.label === "Joints" ? ( // Render specific layout for "Joints" section
                        <TableBody>
                          {section.data.map((row, rowIndex) => (
                            <TableRow
                              key={rowIndex}
                              sx={{
                                height: "44px",
                                color: "#101828",
                                fontFamily: "Poppins",
                              }}
                            >
                              {row.map((cell, cellIndex) => (
                                <TableCell
                                  key={cellIndex}
                                  align="center"
                                  sx={{
                                    padding: "0px 15px",
                                    fontWeight: "500",
                                    fontSize: "14px",

                                    border: "1px solid rgba(51, 51, 51, 0.17)",
                                  }}
                                >
                                  <input
                                    type="text"
                                    value={cell}
                                    style={{
                                      border: "none",
                                      outline: "none",
                                      width: "100%",
                                      fontFamily: "Poppins",
                                    }}
                                    onChange={(e) => {
                                      const updatedJoints = section.data.map(
                                        (r, i) =>
                                          i === rowIndex
                                            ? r.map((c, j) =>
                                                j === cellIndex
                                                  ? e.target.value
                                                  : c
                                              )
                                            : r
                                      );
                                      section.setData(updatedJoints);
                                    }}
                                    // onChange={(e) => {
                                    //   const updatedJoints = [...section.data];
                                    //   updatedJoints[rowIndex][cellIndex] =
                                    //     e.target.value;
                                    //   section.setData(updatedJoints);
                                    // }}
                                  />
                                </TableCell>
                              ))}
                            </TableRow>
                          ))}
                        </TableBody>
                      ) : (
                        <TableBody sx={{ marginTop: "30px" }}>
                          {section.data.map((value, rowIndex) => (
                            <TableRow
                              key={rowIndex}
                              sx={{
                                height: "44px",
                                color: "#101828",
                                fontFamily: "Poppins",
                              }}
                            >
                              <TableCell
                                align="center"
                                sx={{
                                  padding: "0px 15px",
                                  fontWeight: "500",
                                  fontSize: "14px",

                                  border: "1px solid rgba(51, 51, 51, 0.17)",
                                }}
                              >
                                <input
                                  type="text"
                                  style={{
                                    border: "none",
                                    outline: "none",
                                    width: "100%",
                                    fontFamily: "Poppins",
                                  }}
                                  value={value}
                                  onChange={(e) => {
                                    const newData = [...section.data];
                                    newData[rowIndex] = e.target.value;
                                    section.setData(newData);
                                  }}
                                />
                                {/* Add input field or contenteditable attribute for editable text */}
                                {/* <input
                                type="text"
                                style={{
                                  border: "none",
                                  outline: "none",
                                  width: "100%",
                                  fontFamily: "Poppins",
                                }}
                                value={rows[index]}
                                onChange={(e) => {
                                  const newRows = [...rows];
                                  newRows[index] = e.target.value;
                                  setRows(newRows);
                                }}
                                // Add onChange handler to update the value when edited
                                // onChange={(e) =>
                                //   handleInputChange(rowIndex, e.target.value)
                                // }
                                // Set the initial value here, you can bind it to a state variable
                                // value={tableData[rowIndex]}
                              /> */}
                              </TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      )}
                    </Table>
                  </div>
                </TableContainer>
                <IconButton onClick={() => handleAddRow(section)}>
                  <img
                    style={{ width: "18px", height: "18px" }}
                    src={addIcon}
                  ></img>
                </IconButton>
              </Grid>
            ))}
          </Grid>

          <Grid sx={{ display: "flex", justifyContent: "center" }}>
            <Button
              onClick={handleSave}
              sx={{
                backgroundImage:
                  "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
                color: "#FFFFFF",
                borderRadius: "50px",
                fontFamily: "Poppins",
                fontSize: "14px",
                fontWeight: "500",
                textTransform: "none",
                width: "140px",
                height: "40px",
              }}
            >
              Save
            </Button>
          </Grid>
          {/* <Grid
            xs={3}
            sx={{
              display: "flex",
              alignItems: "flex-start",
            }}
          >
            <TableContainer>
              <div style={{ marginBottom: "2px" }}>
                <Table padding={"none"}>
                  <TableHead sx={{ height: "35px", marginBottom: "30px" }}>
                    <TableRow>
                      <TableCell
                        sx={{
                          background:
                            "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
                          color: "#FFFFFF",
                          borderRadius: "10px",
                        }}
                      >
                        <Box
                          sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                          }}
                        >
                          <Typography
                            sx={{
                              fontFamily: "Poppins",
                              fontWeight: "Bold",
                              fontSize: "14px",
                            }}
                          >
                            Joints
                          </Typography>
                        </Box>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                </Table>
              </div>

              <div style={{ marginTop: "2px" }}>
                <Table>
                  <TableBody sx={{ marginTop: "30px" }}>
                    {jointsRows.map((_, index) => (
                      <TableRow
                        // key={ind}
                        sx={{
                          height: "44px",
                          color: "#101828",
                          fontFamily: "Poppins",
                        }}
                      >
                        <TableCell
                          align="center"
                          sx={{
                            padding: "0px 15px",
3                            fontWeight: "500",
                            fontSize: "14px",

                            border: "1px solid rgba(51, 51, 51, 0.17)",
                          }}
                        >
                          <input
                            type="text"
                            style={{
                              border: "none",
                              outline: "none",
                              width: "100%",
                              fontFamily: "Poppins",
                            }}
                            value={jointsRows[index]}
                            onChange={(e) => {
                              const newRows = [...jointsRows];
                              newRows[index] = e.target.value;
                              setJointsRows(newRows);
                            }}
                          />
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </div>
            </TableContainer>
            <IconButton onClick={handleJointRow}>
              <img src={addIcon}></img>
            </IconButton>
          </Grid>
          <Grid
            xs={3}
            sx={{
              display: "flex",
              alignItems: "flex-start",
            }}
          >
            <TableContainer>
              <div style={{ marginBottom: "2px" }}>
                <Table padding={"none"}>
                  <TableHead sx={{ height: "35px", marginBottom: "30px" }}>
                    <TableRow>
                      <TableCell
                        sx={{
                          background:
                            "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
                          color: "#FFFFFF",
                          borderRadius: "10px",
                        }}
                      >
                        <Box
                          sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                          }}
                        >
                          <Typography
                            sx={{
                              fontFamily: "Poppins",
                              fontWeight: "Bold",
                              fontSize: "14px",
                            }}
                          >
                            Pivot Joints
                          </Typography>
                        </Box>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                </Table>
              </div>

              <div style={{ marginTop: "2px" }}>
                <Table>
                  <TableBody sx={{ marginTop: "30px" }}>
                    {pivotsRows.map((_, index) => (
                      <TableRow
                        // key={ind}
                        sx={{
                          height: "44px",
                          color: "#101828",
                          fontFamily: "Poppins",
                        }}
                      >
                        <TableCell
                          align="center"
                          sx={{
                            padding: "0px 15px",
                            fontWeight: "500",
                            fontSize: "14px",

                            border: "1px solid rgba(51, 51, 51, 0.17)",
                          }}
                        >
                          <input
                            type="text"
                            style={{
                              border: "none",
                              outline: "none",
                              width: "100%",
                              fontFamily: "Poppins",
                            }}
                            value={pivotsRows[index]}
                            onChange={(e) => {
                              const newRows = [...pivotsRows];
                              newRows[index] = e.target.value;
                              setPivotsRows(newRows);
                            }}
                            // Add onChange handler to update the value when edited
                            // onChange={(e) =>
                            //   handleInputChange(rowIndex, e.target.value)
                            // }
                            // Set the initial value here, you can bind it to a state variable
                            // value={tableData[rowIndex]}
                          />
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </div>
            </TableContainer>
            <IconButton onClick={handlePivotRow}>
              <img src={addIcon}></img>
            </IconButton>
          </Grid>
          <Grid
            xs={3}
            sx={{
              display: "flex",
              alignItems: "flex-start",
            }}
          >
            <TableContainer>
              <div style={{ marginBottom: "2px" }}>
                <Table padding={"none"}>
                  <TableHead sx={{ height: "35px", marginBottom: "30px" }}>
                    <TableRow>
                      <TableCell
                        sx={{
                          background:
                            "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
                          color: "#FFFFFF",
                          borderRadius: "10px",
                        }}
                      >
                        <Box
                          sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                          }}
                        >
                          <Typography
                            sx={{
                              fontFamily: "Poppins",
                              fontWeight: "Bold",
                              fontSize: "14px",
                            }}
                          >
                            Options/Type
                          </Typography>
                        </Box>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                </Table>
              </div>

              <div style={{ marginTop: "2px" }}>
                <Table>
                  <TableBody sx={{ marginTop: "30px" }}>
                    {optionsRows.map((_, index) => (
                      <TableRow
                        // key={ind}
                        sx={{
                          height: "44px",
                          color: "#101828",
                          fontFamily: "Poppins",
                        }}
                      >
                        <TableCell
                          align="center"
                          sx={{
                            padding: "0px 15px",
                            fontWeight: "500",
                            fontSize: "14px",

                            border: "1px solid rgba(51, 51, 51, 0.17)",
                          }}
                        >
                          <input
                            type="text"
                            style={{
                              border: "none",
                              outline: "none",
                              width: "100%",
                              fontFamily: "Poppins",
                            }}
                            value={optionsRows[index]}
                            onChange={(e) => {
                              const newRows = [...optionsRows];
                              newRows[index] = e.target.value;
                              setOptionsRows(newRows);
                            }}
                            // Add onChange handler to update the value when edited
                            // onChange={(e) =>
                            //   handleInputChange(rowIndex, e.target.value)
                            // }
                            // Set the initial value here, you can bind it to a state variable
                            // value={tableData[rowIndex]}
                          />
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </div>
            </TableContainer>
            <IconButton onClick={handleOptionRow}>
              <img src={addIcon}></img>
            </IconButton>
          </Grid> */}
        </Grid>
      </Grid>
    </Box>
  );
};

export default AddProcedure;
