import React, { useState } from "react";
import { Box, Typography, Grid, TextField, Button } from "@mui/material";
import axios from "axios";
import IcfDetailsAndCoreset from "./IcfDetailsAndCoreset";

const AddIcfcase2 = ({ formData, onBack, onICFandCoreset }) => {
  const [isGenerated, setIsGenerated] = useState(false);
  const [isNextPage, setIsNextPage] = useState(false);

  const [caseData, setCaseData] = useState({
    CaseData: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setCaseData({ ...caseData, [name]: value });
  };

  const prompt = { ...formData, ...caseData };
  // console.log(prompt);

  const finaldataString = JSON.stringify(prompt);

  const allfinaldata = {
    prompt: finaldataString,
  };

  const handleGenerateClick = async () => {
    //combining and sending data together
    // const prompt = { ...formData, ...caseData };
    // // console.log(formData);
    // // console.log(prompt);

    // const token = localStorage.getItem("token");

    // if (!token) {
    //   alert("Token not found. Please log in.");
    //   return;
    // }

    // try {
    //   const response = await axios.post(
    //     "https://procedures.huelogics.com/create_icf_core_set_record",
    //     JSON.stringify(allfinaldata),
    //     {
    //       headers: {
    //         "Content-Type": "application/json",
    //         Authorization: `Bearer ${token}`, // Include the token here
    //       },
    //     }
    //   );

    //   if (response.status === 200) {
    //     alert("Data has been saved successfully!");
    //   }

    //   console.log("Record created:", response.data);
    // } catch (error) {
    //   if (error.response) {
    //     if (error.response.status === 401) {
    //       alert("Invalid Token Or Token Not found");
    //     } else if (error.response.status === 400) {
    //       console.error("Bad Request:", error.response.data);
    //     } else {
    //       console.error("Error:", error.response.status, error.response.data);
    //     }
    //   } else if (error.request) {
    //     // The request was made but no response was received
    //     console.error("No response received:", error.request);
    //   } else {
    //     // Something happened in setting up the request that triggered an Error
    //     console.error("Error", error.message);
    //   }
    // }
    setIsGenerated(true);
  };

  const handleNextToICFandCoreset = () => {
    setIsNextPage(true);
  };

  if (isNextPage) {
    return <IcfDetailsAndCoreset />;
  }

  return (
    <div>
      <Typography
        sx={{
          width: "166px",
          height: "18px",
          fontWeight: 700,
          fontSize: "24px",
          fontFamily: "Montserrat",
          padding: "20px",
          color: "black",
        }}
      >
        Case:
      </Typography>
      <Box sx={{ flexGrow: 1, paddingLeft: 4, paddingRight: 4 }}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            fullWidth
            id="pastMedicalHistory"
            name="pastMedicalHistory"
            multiline
            rows={20}
            sx={{
              mt: "10px",
              "& .MuiInput-underline:after": {
                borderBottomColor: "#000000",
              },
              boxShadow:
                "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
            }}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12} sx={{ textAlign: "center", mt: 3 }}>
          <Button
            onClick={onBack}
            sx={{
              background: "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
              borderRadius: 10,
              px: 4,
              fontSize: "16px",
              color: "#ffffff",
              textTransform: "none",
            }}
          >
            Back
          </Button>
          <Button
            onClick={handleGenerateClick}
            sx={{
              m: "10px",
              background: "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
              borderRadius: 10,
              px: 4,
              fontSize: "16px",
              color: "#ffffff",
              textTransform: "none",
            }}
          >
            Generate
          </Button>
          <Button
            sx={{
              m: "10px",
              background: "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
              borderRadius: 10,
              px: 4,
              fontSize: "16px",
              color: "#ffffff",
              textTransform: "none",
            }}
            onClick={handleNextToICFandCoreset}
            disabled={!isGenerated}
          >
            Next to ICF Details and Core Set
          </Button>
        </Grid>
      </Box>
    </div>
  );
};

export default AddIcfcase2;
