import React, { useState } from "react";
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Box,
  Grid,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import DeleteIcon from "@mui/icons-material/Delete";
import AddIcon from "@mui/icons-material/Add";

const Add = () => {
  const initialSections = [
    { data: [""], label: "Landmarks" },
    { data: [["", ""]], label: "Joints" },
    { data: [""], label: "Pivot Joints" },
    { data: [""], label: "Options/Type" },
  ];

  const [accordions, setAccordions] = useState([
    { label: "", sections: initialSections },
  ]);

  const handleAddAccordion = () => {
    setAccordions([...accordions, { label: "", sections: initialSections }]);
  };

  const handleDeleteAccordion = (index) => {
    setAccordions(accordions.filter((_, i) => i !== index));
  };

  const handleLabelChange = (index, newLabel) => {
    const updatedAccordions = accordions.map((accordion, i) =>
      i === index ? { ...accordion, label: newLabel } : accordion
    );
    setAccordions(updatedAccordions);
  };

  const handleSectionDataChange = (accordionIndex, sectionIndex, newData) => {
    const updatedAccordions = accordions.map((accordion, i) => {
      if (i === accordionIndex) {
        const updatedSections = accordion.sections.map((section, j) =>
          j === sectionIndex ? { ...section, data: newData } : section
        );
        return { ...accordion, sections: updatedSections };
      }
      return accordion;
    });
    setAccordions(updatedAccordions);
  };

  const handleAddRow = (accordionIndex, sectionIndex) => {
    const updatedAccordions = accordions.map((accordion, i) => {
      if (i === accordionIndex) {
        const updatedSections = accordion.sections.map((section, j) => {
          if (j === sectionIndex) {
            const newData =
              section.label === "Joints"
                ? [...section.data, ["", ""]]
                : [...section.data, ""];
            return { ...section, data: newData };
          }
          return section;
        });
        return { ...accordion, sections: updatedSections };
      }
      return accordion;
    });
    setAccordions(updatedAccordions);
  };

  return (
    <Grid container spacing={2}>
      {accordions.map((accordion, accordionIndex) => (
        <Grid item xs={12} key={accordionIndex}>
          <Accordion>
            <AccordionSummary
              expandIcon={<ArrowDropDownIcon />}
              aria-controls={`panel${accordionIndex}-content`}
              id={`panel${accordionIndex}-header`}
            >
              <input
                type="text"
                value={accordion.label}
                onChange={(e) =>
                  handleLabelChange(accordionIndex, e.target.value)
                }
                placeholder="Enter label here"
                style={{
                  border: "none",
                  outline: "none",
                  width: "100%",
                  height: "100%",
                  padding: "0px",
                  margin: "0px",
                  fontSize: "15px",
                  fontFamily: "Inter",
                }}
              />
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={2}>
                {accordion.sections.map((section, sectionIndex) => (
                  <Grid item xs={3} key={sectionIndex}>
                    <TableContainer>
                      <Table padding="none">
                        <TableHead>
                          <TableRow>
                            <TableCell
                              sx={{
                                background:
                                  "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
                                color: "#FFFFFF",
                                borderRadius: "10px",
                              }}
                            >
                              <Box display="flex" justifyContent="center">
                                <Typography
                                  sx={{
                                    fontFamily: "Inter",
                                    fontWeight: "Bold",
                                    fontSize: "14px",
                                  }}
                                >
                                  {section.label}
                                </Typography>
                              </Box>
                            </TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {section.label === "Joints"
                            ? section.data.map((row, rowIndex) => (
                                <TableRow
                                  key={rowIndex}
                                  sx={{
                                    height: "44px",
                                    color: "#101828",
                                    fontFamily: "Inter",
                                  }}
                                >
                                  {row.map((cell, cellIndex) => (
                                    <TableCell
                                      key={cellIndex}
                                      align="center"
                                      sx={{
                                        padding: "0px 15px",
                                        fontWeight: "500",
                                        fontSize: "14px",
                                        border:
                                          "1px solid rgba(51, 51, 51, 0.17)",
                                      }}
                                    >
                                      <input
                                        type="text"
                                        value={cell}
                                        style={{
                                          border: "none",
                                          outline: "none",
                                          width: "100%",
                                          fontFamily: "Inter",
                                        }}
                                        onChange={(e) => {
                                          const updatedJoints =
                                            section.data.map((r, i) =>
                                              i === rowIndex
                                                ? r.map((c, j) =>
                                                    j === cellIndex
                                                      ? e.target.value
                                                      : c
                                                  )
                                                : r
                                            );
                                          handleSectionDataChange(
                                            accordionIndex,
                                            sectionIndex,
                                            updatedJoints
                                          );
                                        }}
                                      />
                                    </TableCell>
                                  ))}
                                </TableRow>
                              ))
                            : section.data.map((value, rowIndex) => (
                                <TableRow
                                  key={rowIndex}
                                  sx={{
                                    height: "44px",
                                    color: "#101828",
                                    fontFamily: "Inter",
                                  }}
                                >
                                  <TableCell
                                    align="center"
                                    sx={{
                                      padding: "0px 15px",
                                      fontWeight: "500",
                                      fontSize: "14px",
                                      border:
                                        "1px solid rgba(51, 51, 51, 0.17)",
                                    }}
                                  >
                                    <input
                                      type="text"
                                      value={value}
                                      style={{
                                        border: "none",
                                        outline: "none",
                                        width: "100%",
                                        fontFamily: "Inter",
                                      }}
                                      onChange={(e) => {
                                        const newData = section.data.map(
                                          (v, i) =>
                                            i === rowIndex ? e.target.value : v
                                        );
                                        handleSectionDataChange(
                                          accordionIndex,
                                          sectionIndex,
                                          newData
                                        );
                                      }}
                                    />
                                  </TableCell>
                                </TableRow>
                              ))}
                        </TableBody>
                      </Table>
                      <IconButton
                        onClick={() =>
                          handleAddRow(accordionIndex, sectionIndex)
                        }
                      >
                        <AddIcon />
                      </IconButton>
                    </TableContainer>
                  </Grid>
                ))}
              </Grid>
            </AccordionDetails>
          </Accordion>
          <IconButton onClick={() => handleDeleteAccordion(accordionIndex)}>
            <DeleteIcon />
          </IconButton>
        </Grid>
      ))}
      <Grid item xs={12}>
        <IconButton onClick={handleAddAccordion}>
          <AddIcon />
        </IconButton>
      </Grid>
    </Grid>
  );
};

export default Add;
