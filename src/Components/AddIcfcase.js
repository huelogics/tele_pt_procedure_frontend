import React, { useState } from "react";
import { Typography, TextField, Grid, Box, Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import AddIcfcase2 from "./AddIcfcase2";

const AddIcfcase = ({ onNextCAsePage }) => {
  const [formData, setFormData] = useState({
    firstName: "",
    middleName: "",
    lastName: "",
    age: "",
    gender: "",
    occupation: "",
    presentingComplaint: "",
    pastMedicalHistory: "",
    socialHistory: "",
  });

  const [isNextPage, setIsNextPage] = useState(false); // State to manage page navigation

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleNext = () => {
    // console.log(formData);
    setIsNextPage(true); // Set to true to render AddIcfcase2
  };

  const handleBack = () => {
    setIsNextPage(false); // Set to false to render the main form
  };
  const handleICFandCoreset = () => {
    setIsNextPage(false);
  };

  if (isNextPage) {
    return (
      <AddIcfcase2
        formData={formData}
        onBack={handleBack}
        onICFandCoreset={handleICFandCoreset}
      />
    );
  }

  return (
    <>
      <Typography
        sx={{
          width: "166px",
          height: "29px",
          fontWeight: 700,
          fontSize: "24px",
          fontFamily: "Montserrat",
          padding: "20px",
          color: "#6F86D6",
        }}
      >
        Patient case
      </Typography>
      <Box
        component="form"
        sx={{ flexGrow: 1, paddingLeft: 4, paddingRight: 4 }}
      >
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography
              sx={{
                fontFamily: "Poppins",
                fontWeight: "400",
                fontSize: "16px",
                marginBottom: "-15px",
              }}
            >
              Name
            </Typography>
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField
              required
              fullWidth
              id="firstName"
              label="First Name"
              name="firstName"
              autoComplete="given-name"
              variant="outlined"
              size="large"
              value={formData.firstName}
              onChange={handleChange}
              sx={{
                mt: "10px",
                width: "100%",
                "& .MuiInput-underline:after": {
                  borderBottomColor: "#000000",
                },
                "& .MuiInputBase-root": {
                  height: "50px",
                },
                boxShadow:
                  "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
              }}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField
              fullWidth
              id="middleName"
              label="Middle Name"
              name="middleName"
              autoComplete="additional-name"
              variant="outlined"
              size="large"
              value={formData.middleName}
              onChange={handleChange}
              sx={{
                mt: "10px",
                width: "100%",
                "& .MuiInputBase-root": {
                  height: "50px",
                },
                "& .MuiInput-underline:after": {
                  borderBottomColor: "#000000",
                },
                boxShadow:
                  "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
              }}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField
              required
              fullWidth
              id="lastName"
              label="Last Name"
              name="lastName"
              autoComplete="family-name"
              variant="outlined"
              size="large"
              value={formData.lastName}
              onChange={handleChange}
              sx={{
                mt: "10px",
                width: "100%",
                "& .MuiInput-underline:after": {
                  borderBottomColor: "#000000",
                },
                "& .MuiInputBase-root": {
                  height: "50px",
                },
                boxShadow:
                  "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
              }}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <Typography
              sx={{
                fontFamily: "Poppins",
                fontWeight: "400",
                fontSize: "16px",
              }}
            >
              Age
            </Typography>
            <TextField
              required
              fullWidth
              id="age"
              name="age"
              type="number"
              variant="outlined"
              size="large"
              value={formData.age}
              onChange={handleChange}
              sx={{
                mt: "10px",
                width: "100%",
                "& .MuiInput-underline:after": {
                  borderBottomColor: "#000000",
                },
                "& .MuiInputBase-root": {
                  height: "50px",
                },
                boxShadow:
                  "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
              }}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <Typography
              sx={{
                fontFamily: "Poppins",
                fontWeight: "400",
                fontSize: "16px",
              }}
            >
              Gender
            </Typography>
            <TextField
              required
              fullWidth
              id="gender"
              name="gender"
              variant="outlined"
              size="large"
              value={formData.gender}
              onChange={handleChange}
              sx={{
                mt: "10px",
                width: "100%",
                "& .MuiInput-underline:after": {
                  borderBottomColor: "#000000",
                },
                "& .MuiInputBase-root": {
                  height: "50px",
                },
                boxShadow:
                  "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
              }}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <Typography
              sx={{
                fontFamily: "Poppins",
                fontWeight: "400",
                fontSize: "16px",
              }}
            >
              Occupation
            </Typography>
            <TextField
              required
              fullWidth
              id="occupation"
              name="occupation"
              variant="outlined"
              size="large"
              value={formData.occupation}
              onChange={handleChange}
              sx={{
                mt: "10px",
                width: "100%",
                "& .MuiInput-underline:after": {
                  borderBottomColor: "#000000",
                },
                "& .MuiInputBase-root": {
                  height: "50px",
                },
                boxShadow:
                  "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <Typography
              sx={{
                fontFamily: "Poppins",
                fontWeight: "400",
                fontSize: "16px",
              }}
            >
              Presenting Complaint
            </Typography>
            <TextField
              required
              fullWidth
              id="presentingComplaint"
              name="presentingComplaint"
              multiline
              rows={1}
              size="large"
              value={formData.presentingComplaint}
              onChange={handleChange}
              sx={{
                mt: "10px",
                width: "100%",

                "& .MuiInput-underline:after": {
                  borderBottomColor: "#000000",
                },
                "& .MuiInputBase-root": {
                  height: "50px",
                },
                boxShadow:
                  "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
              }}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <Typography
              sx={{
                fontFamily: "Poppins",
                fontWeight: "400",
                fontSize: "16px",
              }}
            >
              Past Medical History
            </Typography>
            <TextField
              required
              fullWidth
              id="pastMedicalHistory"
              name="pastMedicalHistory"
              multiline
              rows={4}
              value={formData.pastMedicalHistory}
              onChange={handleChange}
              sx={{
                mt: "10px",
                width: "100%",
                "& .MuiInput-underline:after": {
                  borderBottomColor: "#000000",
                },

                boxShadow:
                  "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
              }}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <Typography
              sx={{
                fontFamily: "Poppins",
                fontWeight: "400",
                fontSize: "16px",
              }}
            >
              Social History
            </Typography>
            <TextField
              required
              fullWidth
              id="socialHistory"
              name="socialHistory"
              multiline
              rows={4}
              value={formData.socialHistory}
              onChange={handleChange}
              sx={{
                mt: "10px",
                width: "100%",
                "& .MuiInput-underline:after": {
                  borderBottomColor: "#000000",
                },
                boxShadow:
                  "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
              }}
            />
          </Grid>
          <Grid item xs={12} sx={{ textAlign: "center", mt: 3 }}>
            <Button
              sx={{
                background:
                  "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
                borderRadius: 10,
                px: 4,
                fontSize: "16px",
                color: "#ffffff",
                textTransform: "none",
              }}
              onClick={handleNext}
            >
              Next
            </Button>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default AddIcfcase;
