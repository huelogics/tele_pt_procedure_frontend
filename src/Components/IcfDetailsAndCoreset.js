import React, { useEffect, useRef, useState } from "react";

import { Box, Button, Grid, Paper, TextField, Typography } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import DownloadIcon from "@mui/icons-material/Download";
import ICFcaselist from "./ICFcaselist";
import axios from "axios";
import { toPng } from "html-to-image";
import { saveAs } from "file-saver";

const IcfDetailsAndCoreset = ({ Onsavebutton }) => {
  const [isNextPage, setIsNextPage] = useState(false);
  const [fetcheddata, setFetcheddata] = useState({});
  const componentRef = useRef(null);

  useEffect(() => {
    const fetchData = async () => {
      const token = localStorage.getItem("token");

      try {
        const res = await axios.get(
          "https://procedures.huelogics.com/get_record_by_case_number/9",
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        // console.log("Fetched data:", res.data);
        setFetcheddata(res.data);
      } catch (error) {
        if (error.response) {
          if (error.response.status === 401) {
            alert("Invalid Token Or Token Not found");
          } else if (error.response.status === 400) {
            console.error("Bad Request:", error.response.data);
          } else {
            console.error("Error:", error.response.status, error.response.data);
          }
        } else if (error.request) {
          // The request was made but no response was received
          console.error("No response received:", error.request);
        } else {
          // Something happened in setting up the request that triggered an Error
          console.error("Error", error.message);
        }
      }
    };

    fetchData();
  }, []); // Empty dependency array to run the effect only once

  if (!fetcheddata.ICF_CASE || !fetcheddata.CoreSets) {
    return (
      <div style={{ textAlign: "center", marginTop: "50px", fontSize: "25px" }}>
        Loading...
      </div>
    );
  }

  console.log(fetcheddata);
  const clientdata = fetcheddata.ICF_CASE?.[0] || {};
  // // console.log(clientdata);
  const name = clientdata.Value?.[1]?.Value || "";
  // // console.log(name);
  const age = clientdata.Value?.[0]?.Value || "";
  // // console.log(age);
  const gender = clientdata.Value?.[3]?.Value || "";
  // // console.log(gender);
  const occupation = clientdata.Value?.[2]?.Value || "";
  // console.log(occupation);

  //------------------------------------------
  const ICFdetails = fetcheddata.ICF_CASE?.[2] || {};
  // console.log(ICFdetails);
  const activities_and_participation = ICFdetails.Value?.[0]?.Value || [];
  // console.log(activities_and_participation);

  const body_functions_and_structures = ICFdetails.Value?.[1]?.Value || [];
  // console.log(body_functions_and_structures);

  const environmental_factors = ICFdetails.Value?.[2].Value || [];
  // console.log(environmental_factors);
  const personal_factors = ICFdetails.Value?.[3]?.Value || [];
  // console.log(personal_factors);

  const formattedBodyFunctionsAndStructures1 = body_functions_and_structures
    .map((item) => `${item.Key}:${item.Value}`)
    .join("\n");
  // console.log(formattedBodyFunctionsAndStructures1);

  const formattedBodyFunctionsAndStructures2 = activities_and_participation
    .map((item) => `${item.Key}:${item.Value}`)
    .join("\n");

  const formattedBodyFunctionsAndStructures3 = environmental_factors
    .map((item) => `${item.Key}:${item.Value}`)
    .join("\n");

  const formattedBodyFunctionsAndStructures4 = personal_factors
    .map((item) => `${item.Key}:${item.Value}`)
    .join("\n");

  const combinedValueoffunctionality = `Body Functions and Structures: 
  ${formattedBodyFunctionsAndStructures1}
  Activities and Participation:
  ${formattedBodyFunctionsAndStructures2}
  Environmental Factors:
  ${formattedBodyFunctionsAndStructures3}
  Personal factors:
  ${formattedBodyFunctionsAndStructures4} `;

  // ------------------------------------------------------------------

  const presenting_issue = fetcheddata.ICF_CASE?.[5] || {};
  // console.log(presenting_issue);

  const icf_qualifiers = fetcheddata.ICF_CASE?.[3] || {};
  // console.log(icf_qualifiers);

  const details_icf = icf_qualifiers.Value || [];
  console.log(details_icf);

  const formatValue = (value) => {
    if (Array.isArray(value)) {
      return value.map((v) => formatValue(v)).join("\n");
    } else if (typeof value === "object" && value !== null) {
      return Object.entries(value)
        .map(([key, val]) => ` ${formatValue(val)}`)
        .join(": ");
    }
    return value;
  };

  const formattedDetails = details_icf
    .map((item) => {
      return `${item.Key}: ${formatValue(item.Value)}`;
    })
    .join("\n");

  //------------------------------------------------------------------------
  const additional_notes = fetcheddata.ICF_CASE?.[6] || {};
  const valuess = additional_notes.Value || [];
  // console.log(valuess);
  // ---------------------------------------------------------------

  const Coreset = fetcheddata.CoreSets?.[0] || {};
  // console.log(Coreset);
  const coresetvalues1 = Coreset.Value?.[2].Value || [];
  const coresetvalues2 = Coreset.Value?.[1].Value || [];
  const coresetvalues3 = Coreset.Value?.[3].Value || [];
  const coresetvalues4 = Coreset.Value?.[0].Value || [];
  // console.log(coresetvalues1, coresetvalues2);

  const handleDownload = () => {
    const node = document.getElementById("capture"); // Adjust the selector to your specific component

    toPng(node)
      .then((dataUrl) => {
        const link = document.createElement("a");
        link.download = "icfdetail_coreset.png";
        link.href = dataUrl;
        link.click();
      })
      .catch((err) => {
        console.log(err);
      });
    alert("Image has been downloaded");
  };

  return (
    <div ref={componentRef} id="capture" style={{ backgroundColor: "white" }}>
      <Box
        sx={{
          width: "100%",
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          gap: "10px",
        }}
      >
        <Typography
          sx={{
            width: "166px",
            height: "29px",
            fontWeight: 700,
            fontSize: "24px",
            fontFamily: "Montserrat",
            padding: "20px",
            color: "#6F86D6",
            marginBottom: "-15px",
          }}
        >
          ICF Details
        </Typography>
        <Grid>
          <Button
            startIcon={<EditIcon />}
            sx={{
              background: "#6F86D6",
              borderRadius: 10,
              px: 2,
              fontSize: "13px",
              color: "#ffffff",
              textTransform: "none",
              m: "5px",
              "&:hover": {
                background: "#6F86D6",
              },
            }}
          >
            Edit
          </Button>
          <Button
            startIcon={<DownloadIcon />}
            sx={{
              background: "#6F86D6",
              borderRadius: 10,
              px: 2,
              fontSize: "13px",
              color: "#ffffff",
              textTransform: "none",
              m: "10px",
              "&:hover": {
                background: "#6F86D6",
              },
            }}
            onClick={handleDownload}
          >
            Download
          </Button>
        </Grid>
      </Box>

      <Box p={3}>
        <Paper
          variant="outlined"
          sx={{ padding: 2, border: "2px solid #6F86D6", borderRadius: "10px" }}
        >
          <Grid container spacing={1}>
            <Grid item xs={12} sm={4}>
              <label
                htmlFor="name"
                style={{ fontFamily: "Poppins", fontWeight: "600" }}
              >
                Name:
              </label>
              <input
                type="text"
                id="name"
                name="name"
                value={name}
                style={{
                  border: "none",
                  fontFamily: "Poppins",
                  fontSize: "16px",
                }}
              />
              <br />
              <label
                htmlFor="age"
                style={{ fontFamily: "Poppins", fontWeight: "600" }}
              >
                Age:
              </label>
              <input
                type="number"
                id="age"
                name="age"
                value={age}
                style={{
                  border: "none",
                  fontFamily: "Poppins",
                  fontSize: "16px",
                }}
              />
              <br />
              <label
                htmlFor="gender"
                style={{ fontFamily: "Poppins", fontWeight: "600" }}
              >
                Gender:
              </label>
              <input
                type="text"
                id="gender"
                name="gender"
                value={gender}
                style={{
                  border: "none",
                  fontFamily: "Poppins",
                  fontSize: "16px",
                }}
              />
              <br />
              <label
                htmlFor="occupation"
                style={{ fontFamily: "Poppins", fontWeight: "600" }}
              >
                Occupation:
              </label>
              <input
                type="text"
                id="occupation"
                name="occupation"
                value={occupation}
                style={{
                  border: "none",
                  fontFamily: "Poppins",
                  fontSize: "16px",
                }}
              />
              <br />
            </Grid>

            <Grid item xs={12} sm={4}>
              <label
                htmlFor="presentingcomplaint"
                style={{ fontFamily: "Poppins", fontWeight: "600" }}
              >
                Presenting Complaint:
              </label>
              <br />
              <textarea
                rows={4}
                type="text"
                id="presentingcomplaint"
                name="presentingcomplaint"
                value={presenting_issue.Value}
                style={{
                  width: "100%",
                  border: "none",
                  fontFamily: "Poppins",
                  fontSize: "16px",
                }}
              />
            </Grid>
          </Grid>
        </Paper>
        {/* ------------------------------------------------------- */}

        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <Typography
              sx={{
                fontFamily: "Poppins",
                fontWeight: "400",
                fontSize: "16px",
                marginTop: "25px",
              }}
            >
              Functionality and Impairability
            </Typography>
            <TextField
              required
              id="functionalityandimpairability"
              name="functionalityandimpairability"
              multiline
              // rows={7}
              value={combinedValueoffunctionality}
              sx={{
                mt: "10px",
                width: "100%",
                "& .MuiInput-underline:after": {
                  borderBottomColor: "#000000",
                },
                "& .MuiOutlinedInput-root": {
                  "& fieldset": {
                    // borderTop: "1px solid grey",
                    // borderBottom: "1px solid grey",
                    borderLeft: "none",
                    borderRight: "none",
                    borderRadius: "0",
                  },
                },
              }}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <Typography
              sx={{
                fontFamily: "Poppins",
                fontWeight: "400",
                fontSize: "16px",
                marginTop: "25px",
              }}
            >
              Qualifiers
            </Typography>
            <TextField
              required
              id="qualifiers"
              name="qualifiers"
              multiline
              rows={17}
              value={formattedDetails}
              sx={{
                mt: "10px",
                width: "100%",
                "& .MuiOutlinedInput-root": {
                  "& fieldset": {
                    borderLeft: "none",
                    borderRight: "none",
                    borderRadius: "0",
                  },
                },
              }}
            />
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Typography
            sx={{
              fontFamily: "Poppins",
              fontWeight: "400",
              fontSize: "16px",
              marginTop: "15px",
            }}
          >
            Additional Details
          </Typography>
          <TextField
            required
            fullWidth
            id="additionaldetails"
            name="additionaldetails"
            multiline
            rows={4}
            value={valuess
              .map((item, index) => {
                return `${item.Key} : ${item.Value}`;
              })
              .join("\n")}
            sx={{
              mt: "10px",
              width: "100%",
              boxShadow:
                "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
            }}
          />
        </Grid>
      </Box>
      <div>
        <Typography
          sx={{
            width: "166px",
            height: "29px",
            fontWeight: 700,
            fontSize: "24px",
            fontFamily: "Montserrat",
            padding: "20px",
            color: "#6F86D6",
            marginBottom: "-15px",
          }}
        >
          Core Set
        </Typography>
        <Typography
          sx={{
            fontWeight: 700,
            fontSize: "15px",
            fontFamily: "Poppins",
            padding: "20px",
            color: "#6F86D6",
          }}
        >
          Comprehensive ICF Core Set
        </Typography>
        <Box sx={{ padding: "20px" }}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6} md={3}>
              <Paper
                sx={{
                  // padding: "10px",
                  borderRadius: "8px",
                  border: "2px solid #6F86D6",
                  boxShadow: "#6F86D6 0px 4px 2px -1px",
                  height: "auto",
                  // height: "420px",
                }}
              >
                <Typography
                  sx={{
                    padding: "10px",
                    fontSize: "20px",
                    fontWeight: "bold",
                    color: "black",
                    borderBottom: "2px solid #6F86D6",
                  }}
                >
                  Body Functions and Structures (b):
                </Typography>
                <Box
                  sx={{
                    borderTop: "2px solid #6F86D6",
                    marginTop: "5px",
                    padding: "10px",
                  }}
                >
                  <TextField
                    // rows={13}
                    fullWidth
                    id="bodyfunctionsandstructures"
                    name="bodyfunctionsandstructures"
                    variant="outlined"
                    size="small"
                    multiline
                    style={{
                      fontFamily: "Poppins",
                      fontSize: "16px",
                    }}
                    value={coresetvalues1
                      .map((item, index) => {
                        return `${item.Key} : ${item.Value}`;
                      })
                      .join("\n")}
                    sx={{
                      marginBottom: "10px",
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          border: "none",
                        },
                      },
                    }}
                  />
                </Box>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Paper
                sx={{
                  // padding: "10px",
                  borderRadius: "8px",
                  border: "2px solid #6F86D6",
                  boxShadow: "#6F86D6 0px 4px 2px -1px",
                  // height: "auto",
                  height: "550px",
                }}
              >
                <Typography
                  sx={{
                    padding: "10px",
                    fontSize: "20px",
                    fontWeight: "bold",
                    color: "black",
                    borderBottom: "2px solid #6F86D6",
                  }}
                >
                  Activities and Participation (d):
                </Typography>
                <Box
                  sx={{
                    borderTop: "2px solid #6F86D6",
                    marginTop: "5px",
                    padding: "10px",
                  }}
                >
                  <TextField
                    // rows={13}
                    fullWidth
                    id="activitiesandparticipations"
                    name="activitiesandparticipations"
                    variant="outlined"
                    size="small"
                    multiline
                    style={{
                      fontFamily: "Poppins",
                      fontSize: "16px",
                    }}
                    value={coresetvalues2
                      .map((item, index) => {
                        return `${item.Key} : ${item.Value}`;
                      })
                      .join("\n")}
                    sx={{
                      marginBottom: "10px",
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          border: "none",
                        },
                      },
                    }}
                  />
                </Box>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Paper
                sx={{
                  // padding: "10px",
                  borderRadius: "8px",
                  border: "2px solid #6F86D6",
                  boxShadow: "#6F86D6 0px 4px 2px -1px",
                  // height: "auto",
                  height: "550px",
                }}
              >
                <Typography
                  sx={{
                    padding: "10px",
                    fontSize: "20px",
                    fontWeight: "bold",
                    color: "black",
                    borderBottom: "2px solid #6F86D6",
                  }}
                >
                  Environmental Factors (e):
                </Typography>
                <Box
                  sx={{
                    borderTop: "2px solid #6F86D6",
                    marginTop: "5px",
                    padding: "10px",
                  }}
                >
                  <TextField
                    rows={13}
                    fullWidth
                    id="environmentalfactors"
                    name="environmentalfactors"
                    variant="outlined"
                    size="small"
                    value={coresetvalues3
                      .map((item, index) => {
                        return `${item.Key} : ${item.Value}`;
                      })
                      .join("\n")}
                    style={{
                      fontFamily: "Poppins",
                      fontSize: "16px",
                    }}
                    multiline
                    sx={{
                      marginBottom: "10px",
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          border: "none",
                        },
                      },
                    }}
                  />
                </Box>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Paper
                sx={{
                  // padding: "10px",
                  borderRadius: "8px",
                  border: "2px solid #6F86D6",
                  boxShadow: "#6F86D6 0px 4px 2px -1px",
                  // height: "auto",
                  height: "550px",
                }}
              >
                <Typography
                  sx={{
                    padding: "10px",
                    fontSize: "20px",
                    fontWeight: "bold",
                    color: "black",
                    borderBottom: "2px solid #6F86D6",
                  }}
                >
                  Personal Factors (p):
                </Typography>
                <Box
                  sx={{
                    borderTop: "2px solid #6F86D6",
                    marginTop: "5px",
                    padding: "10px",
                  }}
                >
                  <TextField
                    rows={13}
                    fullWidth
                    id="personalfactors"
                    name="personalfactors"
                    variant="outlined"
                    multiline
                    size="small"
                    value={coresetvalues4
                      .map((item, index) => {
                        return `${item.Key} : ${item.Value}`;
                      })
                      .join("\n")}
                    style={{
                      fontFamily: "Poppins",
                      fontSize: "16px",
                    }}
                    sx={{
                      marginBottom: "10px",
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          border: "none",
                        },
                      },
                    }}
                  />
                </Box>
              </Paper>
            </Grid>
          </Grid>
        </Box>
        {/* <Typography
          sx={{
            fontWeight: 700,
            fontSize: "15px",
            fontFamily: "Poppins",
            padding: "20px",
            color: "#6F86D6",
          }}
        >
          Brief ICF Core Set
        </Typography>
        <Box sx={{ padding: "15px" }}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6} md={3}>
              <Paper
                sx={{
                  // padding: "10px",
                  borderRadius: "8px",
                  border: "2px solid #6F86D6",
                  boxShadow: "#6F86D6 0px 4px 2px -1px",
                  height: "290px",
                }}
              >
                <Typography
                  sx={{
                    padding: "10px",
                    fontSize: "20px",
                    fontWeight: "bold",
                    color: "black",
                    borderBottom: "2px solid #6F86D6",
                  }}
                >
                  Body Functions and Structures (b):
                </Typography>
                <Box
                  sx={{
                    borderTop: "2px solid #6F86D6",
                    marginTop: "5px",
                    padding: "10px",
                  }}
                >
                  <TextField
                    rows={7}
                    fullWidth
                    variant="outlined"
                    size="small"
                    multiline
                    sx={{
                      marginBottom: "10px",
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          border: "none",
                        },
                      },
                    }}
                  />
                </Box>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Paper
                sx={{
                  // padding: "10px",
                  borderRadius: "8px",
                  border: "2px solid #6F86D6",
                  boxShadow: "#6F86D6 0px 4px 2px -1px",
                  height: "290px",
                }}
              >
                <Typography
                  sx={{
                    padding: "10px",
                    fontSize: "20px",
                    fontWeight: "bold",
                    color: "black",
                    borderBottom: "2px solid #6F86D6",
                  }}
                >
                  Activities and Participation (d):
                </Typography>
                <Box
                  sx={{
                    borderTop: "2px solid #6F86D6",
                    marginTop: "5px",
                    padding: "10px",
                  }}
                >
                  <TextField
                    rows={7}
                    fullWidth
                    variant="outlined"
                    size="small"
                    multiline
                    sx={{
                      marginBottom: "10px",
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          border: "none",
                        },
                      },
                    }}
                  />
                </Box>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Paper
                sx={{
                  // padding: "10px",
                  borderRadius: "8px",
                  border: "2px solid #6F86D6",
                  boxShadow: "#6F86D6 0px 4px 2px -1px",
                  height: "290px",
                }}
              >
                <Typography
                  sx={{
                    padding: "10px",
                    fontSize: "20px",
                    fontWeight: "bold",
                    color: "black",
                    borderBottom: "2px solid #6F86D6",
                  }}
                >
                  Environmental Factors (e):
                </Typography>
                <Box
                  sx={{
                    borderTop: "2px solid #6F86D6",
                    marginTop: "5px",
                    padding: "10px",
                  }}
                >
                  <TextField
                    rows={7}
                    fullWidth
                    variant="outlined"
                    size="small"
                    multiline
                    sx={{
                      marginBottom: "10px",
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          border: "none",
                        },
                      },
                    }}
                  />
                </Box>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Paper
                sx={{
                  // padding: "10px",
                  borderRadius: "8px",
                  border: "2px solid #6F86D6",
                  boxShadow: "#6F86D6 0px 4px 2px -1px",
                  height: "290px",
                }}
              >
                <Typography
                  sx={{
                    padding: "10px",
                    fontSize: "20px",
                    fontWeight: "bold",
                    color: "black",
                    borderBottom: "2px solid #6F86D6",
                  }}
                >
                  Personal Factors (p):
                </Typography>
                <Box
                  sx={{
                    borderTop: "2px solid #6F86D6",
                    marginTop: "5px",
                    padding: "10px",
                  }}
                >
                  <TextField
                    rows={7}
                    fullWidth
                    variant="outlined"
                    multiline
                    size="small"
                    sx={{
                      marginBottom: "10px",
                      "& .MuiOutlinedInput-root": {
                        "& fieldset": {
                          border: "none",
                        },
                      },
                    }}
                  />
                </Box>
              </Paper>
            </Grid>
          </Grid>
        </Box> */}
        <Grid item xs={12} sx={{ textAlign: "center", mt: 3 }}>
          <Button
            onClick={Onsavebutton}
            sx={{
              m: 2,
              background: "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
              borderRadius: 10,
              px: 4,
              fontSize: "16px",
              color: "#ffffff",
              textTransform: "none",
            }}
          >
            Save
          </Button>
        </Grid>
      </div>
    </div>
  );
};

export default IcfDetailsAndCoreset;
