import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Signin from "./Auth/Signin";
import Signup from "./Auth/Signup";
import Dashboard from "./Components/Dashboard";
import { Authorized } from "./middleware/auth";
import { Authorized2 } from "./middleware/auth2";
import AddIcfcase2 from "./Components/AddIcfcase2";
// import AddIcfcase from "./Components/AddIcfcase";
const App = () => {
  return (
    <div className="App">
      <Router>
        {/* <LogoutPrompt /> */}
        <Routes>
          <Route
            path="/"
            element={
              <Authorized2>
                <Signin />
              </Authorized2>
            }
          />
          <Route
            path="/signup"
            element={
              <Authorized2>
                <Signup />
              </Authorized2>
            }
          />
          <Route
            path="/dashboard"
            element={
              <Authorized>
                <Dashboard />
              </Authorized>
            }
          />

          {/* <Route path="/addicfcase2" element={<AddIcfcase2 />} /> */}
        </Routes>
      </Router>
    </div>
    // <Box
    //   sx={{
    //     height: "100vh",
    //     width: "100vw",
    //     padding: "0px",
    //     margin: "0px",
    //   }}
    // >
    //   <Grid
    //     container
    //     xs={12}
    //     sx={{
    //       display: "flex",
    //       flexDirection: "row",
    //     }}
    //   >
    //     <Grid
    //       item
    //       xs={7.5}
    //       sx={{
    //         height: "100vh",
    //         display: "flex",
    //         flexDirection: "column",
    //         justifyContent: "center",
    //         alignItems: "center",
    //         // width: "100vw"
    //       }}
    //     >
    //       <Typography
    //         sx={{
    //           fontFamily: "Poppins",
    //           fontWeight: "600",
    //           fontSize: "36px",
    //           color: "#1D1D1D",
    //           lineHeight: "140%",
    //           letterSpacing: "-2%",
    //           mb: "67px",
    //         }}
    //       >
    //         Welcome
    //       </Typography>
    //       <LoginForm />
    //     </Grid>

    //     <Grid
    //       // container
    //       xs={4.5}
    //       sx={{
    //         height: "100vh",
    //         // width: "100vw",
    //         // width: "581.68px",
    //         background: "linear-gradient(to right, #48C6EF 0%, #6F86D6 100%)",
    //         display: "flex",
    //         flexDirection: "column",
    //         // justifyContent: "center",
    //         // alignItems: "center",
    //       }}
    //     >
    //       {/* <Grid item xs={12} sx={{ backgroundColor: "yellowgreen" }}> */}
    //       <div>
    //         <img src={companyLogo}></img>
    //       </div>
    //       {/* </Grid> */}
    //       <Grid item xs={12}>
    //         <Typography
    //           textAlign="center"
    //           sx={{
    //             fontFamily: "Poppins",
    //             fontSize: "24px",
    //             fontWeight: "600",
    //             color: "#FFFFFF",
    //           }}
    //         >
    //           Don't Have an Account
    //         </Typography>
    //         <Typography
    //           textAlign="center"
    //           sx={{
    //             fontFamily: "Poppins",
    //             fontSize: "24px",
    //             fontWeight: "600",
    //             color: "#FFFFFF",
    //           }}
    //         >
    //           Please Sign Up
    //         </Typography>
    //       </Grid>
    //       {/* <Grid item xs={12}> */}
    //       <Box sx={{ flexGrow: 1 }} />
    //       <Box textAlign="center">
    //         <Button
    //           sx={{
    //             width: "225px",
    //             height: "96px",
    //             border: "1px solid #FFFFFF",
    //             borderRadius: "40px",
    //             mb: "108px",
    //           }}
    //         >
    //           <Typography
    //             sx={{
    //               fontFamily: "Poppins",
    //               fontSize: "32px",
    //               fontWeight: "600",
    //               color: "#FFFFFF",
    //               textTransform: "none",
    //             }}
    //           >
    //             Sign Up
    //           </Typography>
    //         </Button>
    //       </Box>
    //       {/* </Grid> */}
    //     </Grid>
    //   </Grid>
    // </Box>
  );
};

export default App;
