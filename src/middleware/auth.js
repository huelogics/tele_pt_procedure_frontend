const { Navigate } = require("react-router-dom");

export const Authorized = ({ children }) => {
  const token = localStorage.getItem("token");
  const refreshToken = localStorage.getItem("refreshtoken");

  if (!token || !refreshToken) {
    return <Navigate to={"/"} replace={true} />;
  }

  return children;
};
