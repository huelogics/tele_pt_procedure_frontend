const { Navigate } = require("react-router-dom");

export const Authorized2 = ({ children }) => {
  if (<Navigate to={"/"} /> || <Navigate to={"/signup"} />) {
    localStorage.removeItem("token");
    localStorage.removeItem("refreshtoken");
  }

  return children;
};
