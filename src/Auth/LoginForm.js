import * as Yup from "yup";
import { useEffect, useState } from "react";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { Form, Formik, Field, ErrorMessage } from "formik";
import axios from "axios";
import {
  Link,
  TextField,
  IconButton,
  InputAdornment,
  Box,
  Grid,
  Checkbox,
  FormControlLabel,
  Typography,
} from "@mui/material";
import { LoadingButton } from "@mui/lab";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
// import API from "../../axios.utils";

// ----------------------------------------------------------------------

export default function LoginForm({ handleToastMessage }) {
  // Hooks and state management
  const navigate = useNavigate();
  const [showPassword, setShowPassword] = useState(false);
  const [incorrectCredentials, setIncorrectCredentials] = useState(false);

  // Initial form values
  const initialValues = {
    email: "",
    password: "",
    showPassword: false,
    // rememberMe: false,
  };

  // Form validation schema using Yup
  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .email("Please enter valid email")
      .required("Email is required"),
    password: Yup.string().required("Password is required"),
  });

  // State for storing authentication token
  const [token, setToken] = useState("");

  // Form submission handler
  const handleSubmit = async (values) => {
    const params = {
      email: values.email,
      password: values.password,
    };

    try {
      // Making a POST request to the login endpoint
      const res = await axios.post(
        "https://procedures.huelogics.com/sign_in",
        params
      );
      console.log("Login Response", res.data);

      // Extracting the authentication token from the response
      const authToken = res.data.AccessToken;
      console.log(authToken);

      const refreshToken = res.data.RefreshToken;
      console.log(refreshToken);

      // Storing the token in localStorage
      localStorage.setItem("token", authToken);
      localStorage.setItem("refreshtoken", refreshToken);

      // Setting the token state and redirecting to the dashboard
      // setToken(authToken);
      // setIncorrectCredentials(false);
      navigate("/dashboard");
    } catch (err) {
      console.error("Error during login", err);
      // Handling login error and redirecting to the home route
      if (
        (err.response &&
          err.response.data &&
          err.response.data.error.includes("email is incorrect")) ||
        err.response.data.error.includes("password is incorrect")
      ) {
        setIncorrectCredentials(true); // Set the error message for incorrect credentials
      } else {
        setIncorrectCredentials(false);
      }
      navigate("/");
    }
  };

  // Using Formik for form management
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validationSchema}
    >
      {({ values, setFieldValue, errors, touched, isSubmitting }) => (
        <Form>
          <Box sx={{ width: "449px" }}>
            <Grid>
              <div>
                <Typography
                  sx={{
                    textAlign: "left",
                    fontFamily: "Montserrat",
                    fontWeight: "500",
                    fontSize: "16px",
                    color: "#333333",
                  }}
                >
                  Email/Phone number
                </Typography>
                <Field
                  onChange={(evt) => {
                    setFieldValue("email", evt.target.value);
                    initialValues.email = evt.target.value;
                  }}
                  as={TextField}
                  variant="outlined"
                  autoComplete="off"
                  name="email"
                  placeholder="Enter your email"
                  fullWidth
                  size="large"
                  sx={{
                    mt: "10px",
                    "& .MuiInput-underline:after": {
                      borderBottomColor: "#000000",
                    },
                    boxShadow:
                      "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
                    // border: "0.1px solid #A9A9A9",
                    // borderRadius: "8px",
                  }}
                  inputProps={{
                    style: {
                      fontFamily: "Montserrat",
                      fontWeight: "400",
                      fontSize: "14px",
                      //   color: "212121",
                      lineHeight: "28.2px",
                    },
                  }}
                  id="input-with-sx"
                />
              </div>
              <div
                style={{
                  color: "red",
                  fontsize: 10,
                  textAlign: "left",
                  lineHeight: 1.5,
                  fontFamily: "Poppins",
                  paddingLeft: 15,
                  fontSize: "11px",
                  height: "20px",
                  fontWeight: "600",
                }}
              >
                {touched.email && errors.email && <ErrorMessage name="email" />}
              </div>
            </Grid>
            <Grid sx={{ mb: "10px" }}>
              <div>
                <Typography
                  sx={{
                    textAlign: "left",
                    fontFamily: "Montserrat",
                    fontWeight: "500",
                    fontSize: "16px",
                    color: "#333333",
                    mt: "18.53px",
                  }}
                >
                  Password
                </Typography>
                <Field
                  onChange={(evt) => {
                    setFieldValue("password", evt.target.value);
                    initialValues.password = evt.target.value;
                  }}
                  as={TextField}
                  variant="outlined"
                  type={showPassword ? "text" : "password"}
                  name="password"
                  placeholder="Enter your password"
                  fullWidth
                  size="large"
                  inputProps={{
                    style: {
                      fontFamily: "Montserrat",
                      fontWeight: "400",
                      fontSize: "14px",
                      //   color: "#212121",
                      lineHeight: "28.2px",
                      //   borderRadius: "50px",
                    },
                  }}
                  sx={{
                    mt: "10.51px",
                    // "& .MuiInput-underline:after": {
                    //   borderBottomColor: "#000000",
                    // },

                    boxShadow:
                      "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
                  }}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="Toggle password visibility"
                          onClick={() => {
                            setFieldValue({
                              ...initialValues,
                              showPassword: !initialValues.showPassword,
                            });
                            setShowPassword(!showPassword);
                          }}
                          onMouseDown={(event) => event.preventDefault()}
                        >
                          {showPassword ? (
                            <VisibilityOutlinedIcon
                              sx={{
                                width: "16px",
                                height: "16px",
                                color: "#BEBEBE",
                              }}
                            />
                          ) : (
                            <VisibilityOutlinedIcon
                              sx={{
                                width: "16px",
                                height: "16px",
                                color: "#BEBEBE",
                                // backgroundColor: "#BEBEBE",
                              }}
                            />
                          )}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </div>
              <div
                style={{
                  color: "red",
                  fontsize: 10,
                  textAlign: "left",
                  lineHeight: 1.5,
                  fontFamily: "Poppins",
                  paddingLeft: 15,
                  fontSize: "11px",
                  height: "20px",
                  fontWeight: "600",
                }}
              >
                {touched.password && errors.password && (
                  <ErrorMessage name="password" />
                )}
              </div>
            </Grid>

            <Grid
              container
              style={{
                display: "flex",
                justifyContent: "left",
                // mt: "24px",
                // marginTop: "-18px",
              }}
            >
              <Link
                href="/forgotpassword"
                style={{
                  fontWeight: 400,
                  fontSize: "14px",
                  lineHeight: "normal",
                  fontFamily: "Poppins",
                  background:
                    "linear-gradient(to right,#48C6EF 0%,#57ADE5 39%,#6F86D6 100%)",
                  WebkitTextFillColor: "transparent",
                  WebkitBackgroundClip: "text",
                  textDecoration: "none",
                }}
              >
                Forgot Password?
              </Link>
              {/* <FormControlLabel
                control={
                  <Checkbox
                    size="small"
                    checked={values.rememberMe}
                    onChange={(e) =>
                      setFieldValue("rememberMe", e.target.checked)
                    }
                    // color="secondary"
                    name="rememberMe"
                    sx={{
                      "&.Mui-checked": {
                        color: "#874da2",
                      },
                    }}
                  />
                }
                label="Remember me"
                sx={{
                  // marginTop: "10px",
                  "& .MuiFormControlLabel-label": {
                    fontFamily: "Zen Kaku Gothic Antique",
                    fontSize: "10.24px",
                    color: "#757575",
                  },
                }}
              /> */}
            </Grid>
          </Box>

          <Grid
            style={{
              display: "flex",
              justifyContent: "center",
              marginTop: "50px",
            }}
          >
            <LoadingButton
              type="submit"
              color="primary"
              style={{
                height: "53px",
                width: "319px",
                // borderRadius: "56px",
                boxShadow: "0px 8px 21px 0px rgba(0, 0, 0, 0.16)",
                background:
                  "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
                color: "#FFFFFF",
                textTransform: "none",
              }}
              variant="contained"
            >
              <Link style={{ textDecoration: "none" }}>
                <span
                  style={{
                    fontWeight: 600,
                    fontSize: "15px",
                    // lineHeight: "22.5px",
                    fontFamily: "Poppins",
                    color: "white",
                  }}
                >
                  Sign In
                </span>
              </Link>
            </LoadingButton>
          </Grid>
          {/* Display error message for incorrect credentials */}
          {incorrectCredentials && (
            <Typography
              align="center"
              sx={{
                color: "red",
                fontSize: "12px",
                fontFamily: "Poppins",
                // paddingLeft: 15,
                // height: "20px",
                marginTop: "25px",
                fontWeight: "600",
              }}
            >
              {incorrectCredentials
                ? "Email or password is incorrect."
                : "Error during login"}
              {/* Incorrect email or password. */}
            </Typography>
          )}
          <Grid></Grid>
        </Form>
      )}
    </Formik>
  );
}
