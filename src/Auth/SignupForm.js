import * as Yup from "yup";
import { useEffect, useState } from "react";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { Form, Formik, Field, ErrorMessage } from "formik";
import axios from "axios";
import {
  Link,
  TextField,
  IconButton,
  InputAdornment,
  Box,
  Grid,
  Checkbox,
  FormControlLabel,
  Typography,
} from "@mui/material";
import { LoadingButton } from "@mui/lab";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
// import API from "../../axios.utils";

// ----------------------------------------------------------------------

export default function SignupForm({ handleToastMessage }) {
  // Hooks and state management
  const navigate = useNavigate();
  const [showPassword, setShowPassword] = useState(false);
  const [incorrectCredentials, setIncorrectCredentials] = useState(false);

  // Initial form values
  const initialValues = {
    name: "",
    email: localStorage.getItem("email") || "",
    password: "",
    organization: "",
    rememberMe: false,
    // email: localStorage.getItem("email") || "",
    // password: "",
    // showPassword: false,
    // rememberMe: false,
  };

  // Form validation schema using Yup
  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Name is required"),
    email: Yup.string()
      .email("Please enter valid email")
      .required("Email is required"),
    password: Yup.string().required("Password is required"),
    organization: Yup.string().required("Organization is required"),
  });

  // State for storing authentication token
  const [token, setToken] = useState("");

  // Form submission handler
  const handleSubmit = async (values) => {
    const params = {
      name: values.name,
      email: values.email,
      password: values.password,
      organization: values.organization,
    };

    try {
      // Making a POST request to the login endpoint
      const res = await axios.post(
        "https://procedures.huelogics.com/sign_up",
        params
      );
      //  const res = await API.post("app/auth/user/login",params)
      console.log("SIGNUP Response", res);

      // Extracting the authentication token from the response
      const authToken = res.data.token;
      console.log("Auth Token", authToken);

      const refreshToken = res.data.refreshToken;
      console.log("Refresh Token", refreshToken);

      // const csrfToken = res.data.csrf_token;
      // console.log("csrf token", csrfToken);

      // Storing the token in localStorage
      // localStorage.setItem("token", authToken);
      // localStorage.setItem("csrftoken", csrfToken);

      // If rememberMe is checked, save the email in localStorage
      if (values.rememberMe) {
        localStorage.setItem("email", values.email);
      } else {
        // If rememberMe is unchecked, clear the email from localStorage
        localStorage.removeItem("email");
      }
      // Setting the token state and redirecting to the dashboard
      // setToken(authToken);
      setIncorrectCredentials(false);
      navigate("/");
    } catch (err) {
      console.error("Error duringsignup", err);
      setIncorrectCredentials(true);
      // Handling login error and redirecting to the home route
      // if (
      //   (err.response &&
      //     err.response.data &&
      //     err.response.data.message === "Invalid Login Credentials.") ||
      //   err.response.data.message === "User not found."
      // ) {
      //   setIncorrectCredentials(true); // Set the error message for incorrect credentials
      // } else {
      //   setIncorrectCredentials(false);
      //   console.error("Error during login", err);
      // }
      //   navigate("/");
    }
  };

  // Using Formik for form management
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validationSchema}
    >
      {({ values, setFieldValue, errors, touched, isSubmitting }) => (
        <Form>
          <Box sx={{ width: "747px" }}>
            <Grid container spacing="27px">
              <Grid item xs={6} sx={{ width: "350px" }}>
                <div>
                  <Typography
                    sx={{
                      textAlign: "left",
                      fontFamily: "Montserrat",
                      fontWeight: "500",
                      fontSize: "16px",
                      color: "#333333",
                    }}
                  >
                    Name
                  </Typography>
                  <Field
                    onChange={(evt) => {
                      setFieldValue("name", evt.target.value);
                      initialValues.name = evt.target.value;
                    }}
                    as={TextField}
                    variant="outlined"
                    autoComplete="off"
                    name="name"
                    placeholder="Enter your name"
                    fullWidth
                    size="large"
                    sx={{
                      mt: "10px",
                      width: "350px",
                      "& .MuiInput-underline:after": {
                        borderBottomColor: "#000000",
                      },
                      boxShadow:
                        "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
                      // border: "0.1px solid #A9A9A9",
                      // borderRadius: "8px",
                    }}
                    inputProps={{
                      style: {
                        fontFamily: "Montserrat",
                        fontWeight: "400",
                        fontSize: "14px",
                        //   color: "212121",
                        lineHeight: "28.2px",
                      },
                    }}
                    // id="input-with-sx"
                  />
                </div>
                <div
                  style={{
                    color: "red",
                    fontsize: 10,
                    textAlign: "left",
                    lineHeight: 1.5,
                    fontFamily: "Poppins",
                    paddingLeft: 15,
                    fontSize: "11px",
                    height: "20px",
                    fontWeight: "600",
                  }}
                >
                  {touched.name && errors.name && <ErrorMessage name="name" />}
                </div>
              </Grid>

              {/* <Grid item xs={6}>
                <div>
                  <Typography
                    sx={{
                      textAlign: "left",
                      fontFamily: "Montserrat",
                      fontWeight: "500",
                      fontSize: "16px",
                      color: "#333333",
                    }}
                  >
                    Last Name
                  </Typography>
                  <Field
                    onChange={(evt) => {
                      setFieldValue("lastName", evt.target.value);
                      initialValues.lastName = evt.target.value;
                    }}
                    as={TextField}
                    variant="outlined"
                    autoComplete="off"
                    name="lastName"
                    placeholder="Enter your last name"
                    fullWidth
                    size="large"
                    sx={{
                      mt: "10px",
                      width: "350px",
                      "& .MuiInput-underline:after": {
                        borderBottomColor: "#000000",
                      },
                      boxShadow:
                        "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
                      // border: "0.1px solid #A9A9A9",
                      // borderRadius: "8px",
                    }}
                    inputProps={{
                      style: {
                        fontFamily: "Montserrat",
                        fontWeight: "400",
                        fontSize: "14px",
                        //   color: "212121",
                        lineHeight: "28.2px",
                      },
                    }}
                    id="input-with-sx"
                  />
                </div>
              </Grid> */}
              <Grid item xs={6}>
                <div>
                  <Typography
                    sx={{
                      textAlign: "left",
                      fontFamily: "Montserrat",
                      fontWeight: "500",
                      fontSize: "16px",
                      color: "#333333",
                    }}
                  >
                    Email
                  </Typography>
                  <Field
                    onChange={(evt) => {
                      setFieldValue("email", evt.target.value);
                      initialValues.email = evt.target.value;
                    }}
                    as={TextField}
                    variant="outlined"
                    autoComplete="off"
                    name="email"
                    placeholder="Enter your email"
                    fullWidth
                    size="large"
                    sx={{
                      mt: "10px",
                      width: "350px",
                      "& .MuiInput-underline:after": {
                        borderBottomColor: "#000000",
                      },
                      boxShadow:
                        "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
                      // border: "0.1px solid #A9A9A9",
                      // borderRadius: "8px",
                    }}
                    inputProps={{
                      style: {
                        fontFamily: "Montserrat",
                        fontWeight: "400",
                        fontSize: "14px",
                        //   color: "212121",
                        lineHeight: "28.2px",
                      },
                    }}
                    // id="input-with-sx"
                  />
                </div>
                <div
                  style={{
                    color: "red",
                    fontsize: 10,
                    textAlign: "left",
                    lineHeight: 1.5,
                    fontFamily: "Poppins",
                    paddingLeft: 15,
                    fontSize: "11px",
                    height: "20px",
                    fontWeight: "600",
                  }}
                >
                  {touched.email && errors.email && (
                    <ErrorMessage name="email" />
                  )}
                </div>
              </Grid>
              {/* <Grid item xs={6}>
                <div>
                  <Typography
                    sx={{
                      textAlign: "left",
                      fontFamily: "Montserrat",
                      fontWeight: "500",
                      fontSize: "16px",
                      color: "#333333",
                    }}
                  >
                    Date of Birth
                  </Typography>
                  <Field
                    onChange={(evt) => {
                      setFieldValue("date", evt.target.value);
                      initialValues.date = evt.target.value;
                    }}
                    as={TextField}
                    variant="outlined"
                    autoComplete="off"
                    name="date"
                    placeholder="Enter Date of Birth  "
                    fullWidth
                    size="large"
                    type="date"
                    sx={{
                      mt: "10px",
                      width: "350px",
                      "& .MuiInput-underline:after": {
                        borderBottomColor: "#000000",
                      },
                      boxShadow:
                        "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
                      // border: "0.1px solid #A9A9A9",
                      // borderRadius: "8px",
                    }}
                    inputProps={{
                      style: {
                        fontFamily: "Montserrat",
                        fontWeight: "400",
                        fontSize: "14px",
                        //   color: "212121",
                        lineHeight: "28.2px",
                      },
                    }}
                    id="input-with-sx"
                  />
                </div>
              </Grid> */}
              <Grid item xs={6}>
                <div>
                  <Typography
                    sx={{
                      textAlign: "left",
                      fontFamily: "Montserrat",
                      fontWeight: "500",
                      fontSize: "16px",
                      color: "#333333",
                      // mt: "18.53px",
                    }}
                  >
                    Password
                  </Typography>
                  <Field
                    onChange={(evt) => {
                      setFieldValue("password", evt.target.value);
                      initialValues.password = evt.target.value;
                    }}
                    as={TextField}
                    variant="outlined"
                    type={showPassword ? "text" : "password"}
                    name="password"
                    placeholder="Enter your password"
                    fullWidth
                    size="large"
                    inputProps={{
                      style: {
                        fontFamily: "Montserrat",
                        fontWeight: "400",
                        fontSize: "14px",
                        //   color: "#212121",
                        lineHeight: "28.2px",
                        //   borderRadius: "50px",
                      },
                    }}
                    sx={{
                      mt: "10.51px",
                      width: "350px",
                      // "& .MuiInput-underline:after": {
                      //   borderBottomColor: "#000000",
                      // },

                      boxShadow:
                        "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
                    }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="Toggle password visibility"
                            onClick={() => {
                              setFieldValue({
                                ...initialValues,
                                showPassword: !initialValues.showPassword,
                              });
                              setShowPassword(!showPassword);
                            }}
                            onMouseDown={(event) => event.preventDefault()}
                          >
                            {showPassword ? (
                              <VisibilityOutlinedIcon
                                sx={{
                                  width: "16px",
                                  height: "16px",
                                  color: "#BEBEBE",
                                }}
                              />
                            ) : (
                              <VisibilityOutlinedIcon
                                sx={{
                                  width: "16px",
                                  height: "16px",
                                  color: "#BEBEBE",
                                  // backgroundColor: "#BEBEBE",
                                }}
                              />
                            )}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                </div>
                <div
                  style={{
                    color: "red",
                    fontsize: 10,
                    textAlign: "left",
                    lineHeight: 1.5,
                    fontFamily: "Poppins",
                    paddingLeft: 15,
                    fontSize: "11px",
                    height: "20px",
                    fontWeight: "600",
                  }}
                >
                  {touched.password && errors.password && (
                    <ErrorMessage name="password" />
                  )}
                </div>
              </Grid>
              <Grid item xs={6} sx={{ width: "350px" }}>
                <div>
                  <Typography
                    sx={{
                      textAlign: "left",
                      fontFamily: "Montserrat",
                      fontWeight: "500",
                      fontSize: "16px",
                      color: "#333333",
                    }}
                  >
                    Organization
                  </Typography>
                  <Field
                    onChange={(evt) => {
                      setFieldValue("organization", evt.target.value);
                      initialValues.organization = evt.target.value;
                    }}
                    as={TextField}
                    variant="outlined"
                    autoComplete="off"
                    name="organization"
                    placeholder="Enter your organization"
                    fullWidth
                    size="large"
                    sx={{
                      mt: "10px",
                      width: "350px",
                      "& .MuiInput-underline:after": {
                        borderBottomColor: "#000000",
                      },
                      boxShadow:
                        "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
                      // border: "0.1px solid #A9A9A9",
                      // borderRadius: "8px",
                    }}
                    inputProps={{
                      style: {
                        fontFamily: "Montserrat",
                        fontWeight: "400",
                        fontSize: "14px",
                        //   color: "212121",
                        lineHeight: "28.2px",
                      },
                    }}
                    // id="input-with-sx"
                  />
                </div>
                <div
                  style={{
                    color: "red",
                    fontsize: 10,
                    textAlign: "left",
                    lineHeight: 1.5,
                    fontFamily: "Poppins",
                    paddingLeft: 15,
                    fontSize: "11px",
                    height: "20px",
                    fontWeight: "600",
                  }}
                >
                  {touched.organization && errors.organization && (
                    <ErrorMessage name="organization" />
                  )}
                </div>
              </Grid>
            </Grid>
            <Grid
              container
              style={{
                display: "flex",
                justifyContent: "left",
                alignItems: "center",
                marginTop: "10px",
                // marginTop: "-18px",
              }}
            >
              <FormControlLabel
                control={
                  <Checkbox
                    size="small"
                    checked={values.rememberMe}
                    onChange={(e) =>
                      setFieldValue("rememberMe", e.target.checked)
                    }
                    // color="secondary"
                    name="rememberMe"
                    sx={{
                      "&.Mui-checked": {
                        color: "#6F86D6",
                      },
                    }}
                  />
                }
                label="Remember me"
                sx={{
                  // marginTop: "10px",
                  "& .MuiFormControlLabel-label": {
                    fontFamily: "Poppins",
                    fontSize: "15px",
                    color: "#2D3748",
                  },
                }}
              />
            </Grid>
          </Box>

          <Grid
            style={{
              display: "flex",
              justifyContent: "center",

              marginTop: "40px",
            }}
          >
            <LoadingButton
              type="submit"
              color="primary"
              style={{
                height: "53px",
                width: "319px",
                // borderRadius: "56px",
                boxShadow: "0px 8px 21px 0px rgba(0, 0, 0, 0.16)",
                background:
                  "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",

                color: "#FFFFFF",
                textTransform: "none",
              }}
              variant="contained"
            >
              <Link style={{ textDecoration: "none" }}>
                <span
                  style={{
                    fontWeight: 600,
                    fontSize: "15px",
                    // lineHeight: "22.5px",
                    fontFamily: "Poppins",
                    color: "white",
                  }}
                >
                  Create account
                </span>
              </Link>
            </LoadingButton>
          </Grid>

          <Grid></Grid>
        </Form>
      )}
    </Formik>
  );
}
