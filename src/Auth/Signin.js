import React from "react";
import { Box, Button, Grid, Typography } from "@mui/material";
import companyLogo from "../assets/companyLogo.png";
import LoginForm from "../Auth/LoginForm";
import { Link, useNavigate } from "react-router-dom";

const Signin = () => {
  const navigate = useNavigate();
  return (
    <Box
      sx={{
        height: "100vh",
        width: "100vw",
        padding: "0px",
        margin: "0px",
      }}
    >
      <Grid
        container
        // xs={12}
        sx={{
          display: "flex",
          flexDirection: "row",
        }}
      >
        <Grid
          item
          xs={7.5}
          sx={{
            height: "100vh",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            // width: "100vw"
          }}
        >
          <Typography
            sx={{
              fontFamily: "Poppins",
              fontWeight: "600",
              fontSize: "36px",
              color: "#1D1D1D",
              lineHeight: "140%",
              letterSpacing: "-2%",
              mb: "77px",
            }}
          >
            Welcome
          </Typography>
          <LoginForm />
        </Grid>

        <Grid
          item
          // container
          xs={4.5}
          sx={{
            height: "100vh",
            // width: "100vw",
            // width: "581.68px",
            background: "linear-gradient(to right,#667EEA 0%, #764BA2 100%)",
            display: "flex",
            flexDirection: "column",
            // justifyContent: "center",
            // alignItems: "center",
          }}
        >
          {/* <Grid item xs={12} sx={{ backgroundColor: "yellowgreen" }}> */}
          <div>
            <img src={companyLogo}></img>
          </div>
          {/* </Grid> */}
          <Grid item xs={12}>
            <Typography
              textAlign="center"
              sx={{
                fontFamily: "Poppins",
                fontSize: "24px",
                fontWeight: "600",
                color: "#FFFFFF",
              }}
            >
              Don't Have an Account
            </Typography>
            <Typography
              textAlign="center"
              sx={{
                fontFamily: "Poppins",
                fontSize: "24px",
                fontWeight: "600",
                color: "#FFFFFF",
              }}
            >
              Please Sign Up
            </Typography>
          </Grid>
          {/* <Grid item xs={12}> */}
          <Box sx={{ flexGrow: 1 }} />
          <Box textAlign="center">
            <Link
              to="/signup"
              // onClick={() => navigate("/signup")}
            >
              <Button
                sx={{
                  width: "175px",
                  height: "65px",
                  border: "1px solid #FFFFFF",
                  borderRadius: "30px",
                  mb: "108px",
                }}
              >
                <Typography
                  sx={{
                    fontFamily: "Poppins",
                    fontSize: "24px",
                    fontWeight: "600",
                    color: "#FFFFFF",
                    textTransform: "none",
                  }}
                >
                  Sign Up
                </Typography>
              </Button>
            </Link>
          </Box>

          {/* </Grid> */}
        </Grid>
      </Grid>
    </Box>
  );
};

export default Signin;
